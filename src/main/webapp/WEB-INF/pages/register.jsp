<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page session="true" %>
<html>
<head>
<title>Registration</title>
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap-3.1.1.min.css" />"
	rel="stylesheet">
</head>
<body onload='document.regForm.firstName.focus();'>
	<div class="container col-md-6 col-md-offset-3">
		<h3 class="page-header">Registration Form</h3>
		<c:if test="${not empty title}">
			<div class="title">${title}</div>
		</c:if>
		<c:if test="${not empty message}">
			<div class="message">${message}</div>
		</c:if>
		<c:if test="${not empty error}">
			<div class="error">${error}</div>
		</c:if>
	</div>
<br><br>
	<div class="container col-md-6 col-md-offset-3">
		<form:form action="register" method="post" commandName="userForm"
			name='regForm' class="form-horizontal">

			<div class="form-group">
				<div class="col-sm-3 control-label">First Name:</div>
				<div class="col-sm-9">
					<form:input path='firstName' class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-3 control-label">Last Name:</div>
				<div class="col-sm-9">
					<form:input path='lastName' class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-3 control-label">Login:</div>
				<div class="col-sm-9">
					<form:input path='username' class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-3 control-label">Password:</div>
				<div class="col-sm-9">
					<form:password path='password' class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-3 control-label">Repeat Password:</div>
				<div class="col-sm-9">
					<input type="password" name="password2" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-3 control-label">Email:</div>
				<div class="col-sm-9">
					<form:input path='email' type="email" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-3 control-label">Sex:</div>
				<div class="col-sm-9">
					<form:select path="sex" class="form-control">
						<form:option value="" label="--- Select ---" />
						<form:options items="${sexList}" />
					</form:select>
				</div>
			</div>
			<input type="submit" value="Register" class="btn btn-primary" />
		</form:form>
	</div>
</body>
</html>