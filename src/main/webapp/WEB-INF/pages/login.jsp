<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page session="true" %>
<html>
<head>
<title>Login Page</title>
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
</head>
<body onload='document.loginForm.username.focus();'>
	<div class="logincontainer"> 
		<div class="title">Login or <a href = "register">Sign Up</a></div>
 <div class="halfcolumn">
		<form name='loginForm'
		  action="<c:url value='/login' />" method='POST'>
 
		<table>
			<tr>
				<td>username:</td>
				<td><input type='text' name='username' class='input'></td>
			</tr>
			<tr>
				<td>password:</td>
				<td><input type='password' name='password' class='input'/></td>
			</tr>
			<tr>
				<td><input name="submit" type="submit"
				  value="submit" class="button"/></td>
				  <td align="right">remember me&nbsp;<input type="checkbox" name="remember-me" /></td>
				
			</tr>
			<tr>
				<td colspan='2'></td>
			</tr>
		  </table>
 
		  <input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
 
		</form>
	</div>
 <div class="halfcolumn">
  		<c:if test="${not empty error}">
			<div class="error">${error}</div>
		</c:if>
		<c:if test="${not empty message}">
			<div class="message">${message}</div>
		</c:if>
		<c:if test="${empty message}">
	  		<c:if test="${empty error}">
				<div class="message">Please login with your username and password</div>
			</c:if>
		</c:if>
 </div>
 </div>
</body>
</html>