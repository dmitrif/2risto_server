<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page session="true"%>
<html>
<head>
<title>Add Geo Object</title>
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap-3.1.1.min.css" />"
	rel="stylesheet">
<script src="http://maps.googleapis.com/maps/api/js"></script>
<script>
	function initialize() {

		//	var position = new google.maps.LatLng(${geoObject.position.lat},${geoObject.position.lon});
		var mapOptions = {
			zoom : 10,
			panControl : false,
			zoomControl : true,
			mapTypeControl : true,
			scaleControl : false,
			streetViewControl : false,
			overviewMapControl : false,
			rotateControl : false
		}
		map = new google.maps.Map(document.getElementById("googleMap"),
				mapOptions);
		google.maps.event.addListener(map, 'click', function(event) {
			document.getElementById('position_lat').value = event.latLng.lat()
					.toFixed(4);
			document.getElementById('position_lon').value = event.latLng.lng()
					.toFixed(4);
			placeMarker(event.latLng);
		});
		marker = new google.maps.Marker({
			map : map
		});
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				initialLocation = new google.maps.LatLng(
						position.coords.latitude, position.coords.longitude);
				map.setCenter(initialLocation);
			});
		}
	}
	function placeMarker(location) {
		marker.setPosition(location);
	}
	google.maps.event.addDomListener(window, 'load', initialize);
</script>
</head>
<body>
	<%@ include file="header.jsp"%>
	<br>
	<div class="container">
		<div class="col-md-4">
			<form:form action="addobject" method="post" commandName="theForm"
				name='addObjectForm'>
				<div class="form-group">
					Title<br>
					<form:input path='title' class="form-control" />
				</div>
				<div class="form-group">
					<div class="col-sm-2 control-label">Type</div>
					<div class="col-sm-10">
						<form:select path="type" class="form-control">
							<form:option value="" label="--- Select ---" />
							<form:options items="${objectTypeList}" />
						</form:select>
					</div>
				</div>
				<div class="form-group">
					Description<br>
					<form:input path='description' class="form-control" />
				</div>
				<div class="form-group">
					Latitude
					<form:input path='position.gps.lat' id='position_lat'
						class="form-control" />
				</div>
				<div class="form-group">
					Longitude
					<form:input path='position.gps.lon' id='position_lon'
						class="form-control" />
				</div>

				<div class="form-group">
					Start Date<br>
					<form:input path='startDate' class="form-control" />
				</div>
				<div class="form-group">
					End Date<br>
					<form:input path='endDate' class="form-control" />
				</div>
				<div class="form-group">
					Tags
					<form:input path='tags' class="form-control" />
				</div>
				<input type="submit" value="Add" class="btn btn-primary" />

			</form:form>
		</div>
		<div class="col-md-8">
			<div id="googleMap" style="height: 500px;"></div>
		</div>
	</div>

</body>
</html>