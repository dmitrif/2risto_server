<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page session="true" %>
<html>
<head>
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap-3.1.1.min.css" />" rel="stylesheet">
<script src="http://maps.googleapis.com/maps/api/js"></script>
<script>
function initialize() {
	var position = new google.maps.LatLng(${user.position.lat},${user.position.lon});
	var mapOptions = {
	  zoom: 5,
	  center: position,
	  panControl:false,
	  zoomControl:true,
	  mapTypeControl:true,
	  scaleControl:false,
	  streetViewControl:false,
	  overviewMapControl:false,
	  rotateControl:false
	}
	var map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);

	var marker = new google.maps.Marker({
	    position: position,
	    title:"${user.username}"
	});
	marker.setMap(map);
}
 google.maps.event.addDomListener(window, 'load', initialize);
</script>
</head>
<body>
<%@ include file="header.jsp" %>
<br>
	<div class="container">
	<div class="col-md-4">
	<h4>${user.username}</h4>
	<table class="table table-striped table-hover">
	<tr><td>Full Name:</td><td>${user.firstName} ${user.lastName}</td></tr>
	<tr><td>Sex:</td><td>${user.sex}</td></tr>
	<tr><td>Registration Date:</td><td><spring:eval expression="user.registrationDate"/></td></tr>
	<tr><td>Rating:</td><td>${user.votes}</td></tr>
	<tr><td>Initial GPS:</td><td>[${user.initialPosition.gps.lat}, ${user.initialPosition.gps.lat}]</td></tr>
	</table>
 	</div>
 	<div class="col-md-8" id="googleMap" style="height:400px;"></div>
 </div>
 
</body>
</html>