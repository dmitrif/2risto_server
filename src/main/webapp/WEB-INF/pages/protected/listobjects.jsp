<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page session="true"%>
<html>
<head>
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap-3.1.1.min.css" />" rel="stylesheet">
</head>
<body>
<%@ include file="header.jsp" %>
	<br>
	<div class="container">
	<table class="table table-striped table-hover">
	<c:forEach items = "${geoObjectList}" var="item">
	<tr><td>
	<a href="objectdetails?objectid=${item.id}">${item.title}</a> (${item.type})</td>
	
	<c:if test="${showAuthor}">
		<td>Author: <a href="userdetails?userid=${item.author.id}">${item.author.username}</a></td>
	</c:if>
	
	<td>Rating: ${item.rating}</td><td>GPS: ${item.position.gps.lat}, ${item.position.gps.lon}</td><td>Comments: ${item.commentsCount}</td></tr>
 	</c:forEach>
	</table>
 </div>
 
 
</body>
</html>