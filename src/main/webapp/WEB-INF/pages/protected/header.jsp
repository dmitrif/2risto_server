	<script>
		function formSubmit() {
			document.getElementById("logoutForm").submit();
		}
		function buildLocationAndSubmit(linkPrefix){
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function(position) {
					window.location.href = linkPrefix + "?lat=" + position.coords.latitude + "&lon=" + position.coords.longitude + "&dst=10";
				});
			}
		}
	</script>
	<div class="container"><c:url value="/logout" var="logoutUrl"/>
	<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>
	<c:if test="${pageContext.request.userPrincipal.name != null}">
		<h3>
			${pageContext.request.userPrincipal.name}    [ <a
				href="javascript:formSubmit()">logout</a> ]
		</h3>
	</c:if>
	
	<a href = "home">Home</a> | <a href = "addobject">Add Geo Object</a> | <a href = "allobjects">All Geo Objects</a>
	 | <a href = "#" onclick="buildLocationAndSubmit('nearbyobjects')">Nearby Geo Objects</a>
	 | <a href = "#" onclick="buildLocationAndSubmit('nearbyusers')">Nearby Users</a>
	<c:if test="${not empty title}">
		<div class="title">${title}</div>
	</c:if>
	<c:if test="${not empty message}">
		<div class="message">${message}</div>
	</c:if>
	<c:if test="${not empty error}">
		<div class="error">${error}</div>
	</c:if>
	</div>