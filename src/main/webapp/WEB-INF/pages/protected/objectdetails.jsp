<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page session="true" %>
<html>
<head>
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap-3.1.1.min.css" />"
	rel="stylesheet">
<script src="http://maps.googleapis.com/maps/api/js"></script>
<script>
function initialize() {
	var position = new google.maps.LatLng(${geoObject.position.gps.lat},${geoObject.position.gps.lon});
	var mapOptions = {
	  zoom: 5,
	  center: position,
	  panControl:false,
	  zoomControl:true,
	  mapTypeControl:true,
	  scaleControl:false,
	  streetViewControl:false,
	  overviewMapControl:false,
	  rotateControl:false
	}
	var map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);

	var marker = new google.maps.Marker({
	    position: position,
	    title:"${geoObject.title}"
	});
	marker.setMap(map);
}
 google.maps.event.addDomListener(window, 'load', initialize);
</script>
</head>
<body>
	<%@ include file="header.jsp"%>
	<br>
	<div class="container">
		<div class="col-md-4">
			<h4>${geoObject.title}</h4>
			<h5><a href="userdetails?userid=${geoObject.author.id}">${geoObject.author.username}</a></h5>
			<table class="table table-striped table-hover">
				<tr>
					<td>Type:</td>
					<td>${geoObject.type}</td>
				</tr>
				<tr>
					<td>Description:</td>
					<td>${geoObject.description}</td>
				</tr>
				<tr>
					<td>Creation Date:</td>
					<td><spring:eval expression="geoObject.date"/></td>
				</tr>
				<c:if test="${not empty geoObject.startDate}">
					<tr>
						<td>Start Date:</td>
						<td><spring:eval expression="geoObject.startDate"/></td>
					</tr>
					<tr>
						<td>End Date:</td>
						<td><spring:eval expression="geoObject.endDate"/></td>
					</tr>
				</c:if>
				<tr>
					<td>Rating:</td>
					<td>${geoObject.rating}</td>
				</tr>
				<tr>
					<td>Comments:</td>
					<td>${geoObject.commentsCount}</td>
				</tr>
				<tr>
					<td>GPS:</td>
					<td>[${geoObject.position.gps.lat}, ${geoObject.position.gps.lon}]</td>
				</tr>
			</table>
		</div>
		<div class="col-md-8" id="googleMap" style="height: 400px;"></div>
	</div>
	<br>
	<c:if test="${commentList.size() > 0}">
		<div class="container">
	Comments:
		<table class="table">
			<c:forEach items="${commentList}" var="item">
				<tr>
				<td>
				
					${item.date} <a href="userdetails?userid=${item.author.id}">${item.author.username}</a>
					<br><div class="borderedbox">
					${item.body}</div>
				
				</td>
				</tr>
			</c:forEach>
		</table>
	</div>
	</c:if>
	<div class="container">
		<form:form action="addcomment" method="post" name='addCommentForm'
			commandName="newcomment" class="form">
			<br>
  	New Comment:
   	<form:textarea path="body" class="form-control" />
			<br>
			<input type="hidden" name="objectid" value="${geoObject.id}" />
			<input type="submit" value="Add" class="btn btn-primary" />
		</form:form>
	</div>
</body>
</html>