<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page session="true"%>
<html>
<head>
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap-3.1.1.min.css" />"
	rel="stylesheet">
</head>
<body>
	<%@ include file="header.jsp"%>
	<br>
	<div class="container">
		<table class="table table-striped table-hover">
			<c:forEach items="${userList}" var="item">
				<tr>
					<td><a href="userdetails?userid=${item.id}">${item.login}</a></td>
					<td>${item.firstName}${item.lastName}</td>
					<td>Rating: ${item.rating}</td>
					<td>GPS: ${item.position.lat}, ${item.position.lon}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>