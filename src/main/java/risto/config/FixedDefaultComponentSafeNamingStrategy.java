package risto.config;

import org.hibernate.ogm.cfg.impl.OgmNamingStrategy;

public class FixedDefaultComponentSafeNamingStrategy extends OgmNamingStrategy {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final FixedDefaultComponentSafeNamingStrategy INSTANCE = new FixedDefaultComponentSafeNamingStrategy();

	@Override
	public String propertyToColumnName(String propertyName) {
		propertyName = propertyName.replace(".collection&&element.", ".");
		return propertyName;
	}

	@Override
	public String foreignKeyColumnName(String propertyName,
			String propertyEntityName, String propertyTableName,
			String referencedColumnName) {
		String columnName = super.foreignKeyColumnName(propertyName,
				propertyEntityName, propertyTableName, referencedColumnName);
		columnName = columnName.replace(".collection&&element.", ".");
		return columnName;
	}
}