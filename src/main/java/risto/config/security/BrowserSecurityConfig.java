package risto.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import risto.config.security.token.PersistentTokenRepositoryImpl;

@Configuration
@EnableWebMvcSecurity
@ComponentScan
@Order(2)
public class BrowserSecurityConfig extends WebSecurityConfigurerAdapter {

//	private String tokenKey = "tristo";

	@Autowired 
	private UserDetailsServiceImpl userDetailsServiceImpl;
	
	@Autowired
	private BrowserAuthenticationSuccessHandler authenticationSuccessHandler;

	@Override
    protected void configure(HttpSecurity http) throws Exception {
  	  http
//  	.a
//	  .rememberMe()
//.rememberMeServices(browserTokenBasedRememberMeService())
//  	  .and()

//		.addFilterBefore(this.browserRememberMeAuthenticationFilter(),
//		BasicAuthenticationFilter.class).sessionManagement()
//		.and()
.authorizeRequests()
  		.antMatchers("/protected/**").access("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
  		.antMatchers("/private/**").access("hasRole('ROLE_ADMIN')")
  		.and()
  		  .formLogin().loginPage("/login").failureUrl("/login?error").successHandler(authenticationSuccessHandler)
		  .usernameParameter("username").passwordParameter("password")
//		.and()
//						.addFilterBefore(this.browserRememberMeAuthenticationFilter(),
//						BasicAuthenticationFilter.class).sessionManagement()
		.and()
		  .logout().logoutSuccessUrl("/login?logout")
		.and()
		  .exceptionHandling().accessDeniedPage("/public/404")
		.and()
		  .csrf()
		  
.and()
		  .rememberMe()
//.rememberMeServices(browserTokenBasedRememberMeService())
		  .tokenRepository(persistentTokenRepository())
			.tokenValiditySeconds(1209600)
			;
    }

	@Bean
	public PersistentTokenRepository persistentTokenRepository() {
		PersistentTokenRepositoryImpl db = new PersistentTokenRepositoryImpl();
		return db;
	}

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    	auth
	 	.userDetailsService(userDetailsServiceImpl)
	 	.passwordEncoder(bCryptPasswordEncoder());
//		auth.authenticationProvider(new RememberMeAuthenticationProvider(
//				tokenKey));
    }

//    @Bean
//    public RememberMeAuthenticationProvider rememberMeAuthenticationProvider() {
//        RememberMeAuthenticationProvider rememberMeAuthenticationProvider = new RememberMeAuthenticationProvider(environment.getProperty("security.key", "test"));
//        return rememberMeAuthenticationProvider;
//    }
    
    public BCryptPasswordEncoder bCryptPasswordEncoder(){
    	return new BCryptPasswordEncoder();
    }
    
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/restlogin", "/test", "/css/**");
    }

//	@Bean
//	public RememberMeAuthenticationFilter browserRememberMeAuthenticationFilter()
//			throws Exception {
//		return new RememberMeAuthenticationFilter(authenticationManager(),
//				browserTokenBasedRememberMeService());
//	}

//	@Bean
//	public BrowserTokenBasedRememberMeService browserTokenBasedRememberMeService() {
//		BrowserTokenBasedRememberMeService service = new BrowserTokenBasedRememberMeService(
//				tokenKey, userDetailsServiceImpl, persistentTokenRepository());
//		return service;
//	}

}
