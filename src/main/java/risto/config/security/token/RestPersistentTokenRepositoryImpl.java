package risto.config.security.token;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("RestTokenRepository")
public class RestPersistentTokenRepositoryImpl extends PersistentTokenRepositoryImpl{

	@Override
	public void updateToken(String series, String tokenValue, Date lastUsed) {
		System.out.println("Tried to update rest token");
	}

	@Override
	@Transactional
	public void createNewToken(PersistentRememberMeToken token) {
		RememberMeToken rememberMeToken = new RememberMeToken(token);
		rememberMeToken.setIsRest(true);
		entityManager.persist(rememberMeToken);
	}

	@Transactional
	public void removeTokenForSeries(String series) {
		RememberMeToken rememberMeToken = entityManager.find(RememberMeToken.class, series);
		if(rememberMeToken != null)
			entityManager.remove(rememberMeToken);
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public void removeUserTokens(String userName) {
		Query query = entityManager.createQuery("from RememberMeToken where username =:param and isRest =:rest")
				.setParameter("param", userName).setParameter("rest", true);
		List<RememberMeToken> ret = query.getResultList();
		for(RememberMeToken obj : ret)
			entityManager.remove(obj);
	}
}
