package risto.config.security.token;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.transaction.annotation.Transactional;

public class PersistentTokenRepositoryImpl implements PersistentTokenRepository {

	@PersistenceContext
	protected EntityManager entityManager;

	@Override
	@Transactional
	public void createNewToken(PersistentRememberMeToken token) {
		RememberMeToken rememberMeToken = new RememberMeToken(token);
		entityManager.persist(rememberMeToken);
	}

	@Override
	@Transactional
	public void updateToken(String series, String tokenValue, Date lastUsed) {
		RememberMeToken rememberMeToken = entityManager.find(RememberMeToken.class, series);
		rememberMeToken.setTokenValue(tokenValue);
		rememberMeToken.setDate(lastUsed);
	}

	@Override
	public PersistentRememberMeToken getTokenForSeries(String seriesId) {
		RememberMeToken rememberMeToken = entityManager.find(RememberMeToken.class, seriesId);
		if(rememberMeToken != null)
			return new PersistentRememberMeToken(rememberMeToken.getUsername(), rememberMeToken.getSeries(), rememberMeToken.getTokenValue(), rememberMeToken.getDate());
		else
			return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public void removeUserTokens(String userName) {
		Query query = entityManager.createQuery("from RememberMeToken where username =:param and isRest =:rest")
				.setParameter("param", userName).setParameter("rest", false);
		List<RememberMeToken> ret = query.getResultList();
		for(RememberMeToken obj : ret)
			entityManager.remove(obj);
	}

}
