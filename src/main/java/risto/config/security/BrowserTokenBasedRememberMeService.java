package risto.config.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.rememberme.InMemoryTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

public class BrowserTokenBasedRememberMeService extends PersistentTokenBasedRememberMeServices{

    private PersistentTokenRepository tokenRepository = new InMemoryTokenRepositoryImpl();
	
	public BrowserTokenBasedRememberMeService(String key, UserDetailsService userDetailsService, PersistentTokenRepository persistentTokenRepositoryImpl) {
		super(key, userDetailsService, persistentTokenRepositoryImpl);
		this.tokenRepository = persistentTokenRepositoryImpl;
	}

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        cancelCookie(request, response);

        if (authentication != null) {
            tokenRepository.removeUserTokens(authentication.getName());
        }
    }

    protected void onLoginSuccess(HttpServletRequest request, HttpServletResponse response, Authentication successfulAuthentication) {
super.onLoginSuccess(request, response, successfulAuthentication);
    	//        String username = successfulAuthentication.getName();
//
//        logger.debug("Creating new persistent login for user " + username);
//
//        PersistentRememberMeToken persistentToken = new PersistentRememberMeToken(username, generateSeriesData(),
//                generateTokenData(), new Date());
//        try {
//            tokenRepository.createNewToken(persistentToken);
//            addCookie(persistentToken, request, response);
//        } catch (DataAccessException e) {
//            logger.error("Failed to save persistent token ", e);
//        }
    }


}