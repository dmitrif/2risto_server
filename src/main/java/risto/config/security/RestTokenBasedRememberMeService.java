package risto.config.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import risto.config.security.token.RestPersistentTokenRepositoryImpl;

public class RestTokenBasedRememberMeService extends PersistentTokenBasedRememberMeServices{

	@Autowired
	@Qualifier("RestTokenRepository")
	private PersistentTokenRepository tokenRepository;

//	@Autowired
//	@Qualifier("RestTokenRepository")
//    private RestPersistentTokenRepositoryImpl tokenRepository;
	
	public RestTokenBasedRememberMeService(String key, UserDetailsService userDetailsService, PersistentTokenRepository persistentTokenRepositoryImpl) {
		super(key, userDetailsService, persistentTokenRepositoryImpl);
//		this.tokenRepository = persistentTokenRepositoryImpl;
	}

	private final String HEADER_SECURITY_TOKEN = "X-RST-Token";

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        cancelCookie(request, response);
        if (logger.isDebugEnabled()) {
            logger.debug("Logout for token '" + request.getHeader(HEADER_SECURITY_TOKEN));
        }
        if (authentication != null) {

            String rememberMeCookie = extractRememberMeCookie(request);
            String[] cookieTokens = decodeCookie(rememberMeCookie);
            final String presentedSeries = cookieTokens[0];
            ((RestPersistentTokenRepositoryImpl)tokenRepository).removeTokenForSeries(presentedSeries);
        }
    }

    public void logoutFromEverywhere(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
    	super.logout(request, response, authentication);
    }

	@Override
	protected String extractRememberMeCookie(HttpServletRequest request) {
		String token = request.getHeader(HEADER_SECURITY_TOKEN);
		if ((token == null) || (token.length() == 0)) {
			return null;
		}
		return token;
	}

	public String getHeaderTokenName() {
		return HEADER_SECURITY_TOKEN;
	}

	private String tokenValue;
	
	public String getCurrentTokenValue(){
		return tokenValue;
	}

	@Override
	protected void setCookie(String[] tokens, int maxAge, HttpServletRequest request, HttpServletResponse response){
        tokenValue = encodeCookie(tokens);
        if (logger.isDebugEnabled()) {
            logger.debug("Set token cookie value '" + tokenValue);
        }
	}		
}