package risto.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.RememberMeAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.authentication.rememberme.RememberMeAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
@EnableWebSecurity
@Order(1)
public class RestSecurityConfig extends WebSecurityConfigurerAdapter {

	private String tokenKey = "tristo";

	@Autowired
	private UserDetailsServiceImpl userDetailsServiceImpl;

	@Autowired
	@Qualifier("RestTokenRepository")
	private PersistentTokenRepository persistentTokenRepositoryImpl;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.antMatcher("/service/**")
				.csrf()
				.disable()
				.authorizeRequests()
				.anyRequest()
				.authenticated()
				.and()
				.addFilterBefore(rememberMeAuthenticationFilter(),
						BasicAuthenticationFilter.class).sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
				.exceptionHandling().accessDeniedPage("/public/403");
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth)
			throws Exception {
		auth.authenticationProvider(new RememberMeAuthenticationProvider(
				tokenKey));
		auth.userDetailsService(userDetailsServiceImpl).passwordEncoder(
				bCryptPasswordEncoder());
	}

	@Bean
	public RememberMeAuthenticationFilter rememberMeAuthenticationFilter()
			throws Exception {
		return new RememberMeAuthenticationFilter(authenticationManager(),
				tokenBasedRememberMeService());
	}

	@Bean
	public RestTokenBasedRememberMeService tokenBasedRememberMeService() {
		RestTokenBasedRememberMeService service = new RestTokenBasedRememberMeService(
				tokenKey, userDetailsServiceImpl, persistentTokenRepositoryImpl);
		service.setAlwaysRemember(true);
		service.setCookieName("risto");
		return service;
	}

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	RememberMeAuthenticationProvider rememberMeAuthenticationProvider() {
		return new RememberMeAuthenticationProvider(tokenKey);
	}

	@Bean(name = "restAuthenticationManager")
	// @Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/restlogin", "/restregister", "/test", "/css/**");
		// web.ignoring().antMatchers("/test");
	}
}