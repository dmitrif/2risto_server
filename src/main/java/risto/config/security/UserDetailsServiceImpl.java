package risto.config.security;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import risto.data.model.user.User;
import risto.data.service.UserService;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	UserService userService;
	
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();
		User user = userService.getUserByUsername(username);
		if (user == null) {
            throw new UsernameNotFoundException("User [" + username + "] not found!");
        }
		setAuths.add(new SimpleGrantedAuthority(user.getRole().toString()));
		List<GrantedAuthority> result = new ArrayList<GrantedAuthority>(setAuths);
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), true, true, true, true, result);
	}

}
