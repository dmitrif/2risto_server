package risto.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class DatabaseConfig {

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setPersistenceProviderClass(org.hibernate.ogm.jpa.HibernateOgmPersistence.class); 
		em.setPackagesToScan(new String[] { "risto.data.model", "risto.config.security.token" });
		em.setJpaProperties(additionalProperties());
		return em;
	}

	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory emf){
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);
		return transactionManager;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation(){
		return new PersistenceExceptionTranslationPostProcessor();
	}

	Properties additionalProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.ogm.datastore.provider","MongoDB");
		properties.setProperty("hibernate.ogm.datastore.database", "risto");
		properties.setProperty("hibernate.ogm.mongodb.host", "localhost");
		properties.setProperty("hibernate.ogm.mongodb.port", "27017");
		properties.setProperty("hibernate.ogm.datastore.create_database", "true");
		return properties;
	}
}