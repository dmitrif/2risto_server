package risto.config.init;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import risto.data.model.geoObject.GeoObjectType;
import risto.data.model.geoObject.LinkedObjectProvider;

@Component
public class ContextRefreshListener implements ApplicationListener<ContextRefreshedEvent> {

	@PersistenceContext
	protected EntityManager entityManager;

	@Override
	@Transactional
	public void onApplicationEvent(final ContextRefreshedEvent event) {
        int num = entityManager.createQuery("from " + GeoObjectType.class.getName()).getResultList().size();
        if(num < 4){
        	if(num > 0){
				List<GeoObjectType> entities = entityManager.createQuery("from " + GeoObjectType.class.getName(), GeoObjectType.class).getResultList();
        		for(GeoObjectType o : entities){
        			entityManager.remove(o);
        		}
        	}
            GeoObjectType event_ = new GeoObjectType();
            event_.setType("EVENT");
            entityManager.persist(event_);
            GeoObjectType buzz = new GeoObjectType();
            buzz.setType("BUZZ");
            entityManager.persist(buzz);
            GeoObjectType place = new GeoObjectType();
            place.setType("PLACE");
            entityManager.persist(place);
            GeoObjectType request = new GeoObjectType();
            request.setType("REQUEST");
            entityManager.persist(request);
        }
        num = entityManager.createQuery("from " + LinkedObjectProvider.class.getName()).getResultList().size();
        if(num < 2){
        	if(num > 0){
				List<LinkedObjectProvider> entities = entityManager.createQuery("from " + LinkedObjectProvider.class.getName(), LinkedObjectProvider.class).getResultList();
        		for(LinkedObjectProvider o : entities){
        			entityManager.remove(o);
        		}
        	}
        	LinkedObjectProvider google = new LinkedObjectProvider();
        	google.setType("GOOGLE");
            entityManager.persist(google);
        	LinkedObjectProvider self = new LinkedObjectProvider();
        	self.setType("SELF");
            entityManager.persist(self);
        }
	}
}