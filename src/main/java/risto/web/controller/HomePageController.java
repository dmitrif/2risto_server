package risto.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import risto.data.model.geoObject.GeoObject;
import risto.data.model.user.User;
import risto.data.service.GeoObjectService;
import risto.data.service.UserService;

@Controller
public class HomePageController {

	@Autowired
	private UserService userService;

	@Autowired
	private GeoObjectService geoObjectService;

	@RequestMapping(value = {"protected/home"}, method = RequestMethod.GET)
	public ModelAndView userGeoObjects() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		User user = userService.getUserByUsername(name);
		List<GeoObject> geoObjects = userService.getGeoObjectsForUserId(user.getId());
		ModelAndView model = new ModelAndView();
		model.addObject("geoObjectList", geoObjects);
		model.setViewName("protected/listobjects");
		return model;
	}
}
