package risto.web.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import risto.data.model.geoObject.GeoObject;
import risto.data.model.user.User;
import risto.data.model.user.UserRef;
import risto.data.service.GeoObjectService;
import risto.data.service.UserService;

@Controller
public class AddGeoObjectController {

	@Autowired
	private UserService userService;

	@Autowired
	private GeoObjectService geoObjectService;

	@RequestMapping(value = { "/protected/addobject" }, method = RequestMethod.POST)
	public ModelAndView addGeoObjectPost(HttpServletRequest request, HttpServletResponse response,
			@Valid @ModelAttribute("theForm") GeoObject geoObject, BindingResult bindingResult,
			ModelAndView modelAndView) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		String name = auth.getName();
		if (geoObject.getTitle().equals("") || geoObject.getType().equals("") || geoObject.getPosition().getGps() == null
				|| geoObject.getPosition().getGps().getLat() == null
				|| geoObject.getPosition().getGps().getLon() == null) {
			modelAndView.addObject("error",
					"Title, Type and Position are obligatory fields");
			modelAndView.setViewName("/protected/addobject");
			return addGeoObjectGet(modelAndView, geoObject);
		}
		if(bindingResult.hasErrors()){
			for (Object object : bindingResult.getAllErrors()) {
			    if(object instanceof FieldError) {
			        FieldError fieldError = (FieldError) object;
					modelAndView.addObject("error", fieldError.getField() + ": " + fieldError.getDefaultMessage());
					modelAndView.setViewName("/protected/addobject");
					return addGeoObjectGet(modelAndView, geoObject);
			    }
			}
		}
		User user = userService.getUserByUsername(name);
		geoObject.setAuthor(new UserRef(user));
		geoObjectService.add(geoObject);
		return new ModelAndView("redirect:/protected/home");
	}

	@RequestMapping(value = { "/protected/addobject" }, method = RequestMethod.GET)
	public ModelAndView addGeoObjectGet(ModelAndView modelAndView,
			@ModelAttribute("theForm") GeoObject geoObject) {
		modelAndView.addObject("theForm", geoObject);
		Map<String, String> objectTypeList = new LinkedHashMap<String, String>();
		objectTypeList.put("PLACE", "Object");
		objectTypeList.put("EVENT", "Event");
		objectTypeList.put("BUZZ", "Buzz");
		objectTypeList.put("REQUEST", "Request");
		modelAndView.addObject("objectTypeList", objectTypeList);
		return modelAndView;
	}
}
