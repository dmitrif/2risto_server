package risto.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import risto.data.model.geoObject.GeoObject;
import risto.data.service.GeoObjectService;

@Controller
public class AllObjectsController {

	@Autowired
	private GeoObjectService geoObjectService;

	@RequestMapping(value = {"protected/allobjects"}, method = RequestMethod.GET)
	public ModelAndView allObjects() {
		List<GeoObject> geoObjects = geoObjectService.getAllGeoObjects();
		ModelAndView model = new ModelAndView();
		model.addObject("geoObjectList", geoObjects);
		model.addObject("showAuthor", true);
		model.setViewName("protected/listobjects");
		return model;
	}
}