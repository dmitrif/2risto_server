package risto.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import risto.data.model.user.User;
import risto.data.service.UserService;

@Controller
public class UserDetailsController {

	@Autowired
	private UserService userService;

	@RequestMapping(value={"/protected/userdetails"}, method = RequestMethod.GET)
	public ModelAndView objectDetails(@RequestParam(value = "userid") String userId, ModelAndView modelAndView){
		User userObject = userService.getUserById(userId);
		modelAndView.addObject("user", userObject);
		return modelAndView;
	}
}
