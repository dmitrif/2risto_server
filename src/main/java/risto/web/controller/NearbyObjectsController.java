package risto.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import risto.data.model.Position;
import risto.data.model.geoObject.GeoObject;
import risto.data.service.GeoObjectService;

@Controller
public class NearbyObjectsController {

	@Autowired
	private GeoObjectService geoObjectService;

	@RequestMapping(value = {"protected/nearbyobjects"}, method = RequestMethod.GET)
	public ModelAndView nearbyObjects(@RequestParam(value = "lon") String lon, @RequestParam(value = "lat") String lat, @RequestParam(value = "dst") String distance) {
		List<GeoObject> geoObjects = geoObjectService.getGeoObjectsInRange(new Position(Double.parseDouble(lon), Double.parseDouble(lat)), Double.parseDouble(distance));
		ModelAndView model = new ModelAndView();
		model.addObject("geoObjectList", geoObjects);
		model.addObject("showAuthor", true);
		model.setViewName("protected/listobjects");
		return model;
	}
}
