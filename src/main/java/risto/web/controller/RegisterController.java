package risto.web.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import risto.data.model.user.User;
import risto.data.service.UserService;
import risto.misc.Constants;

@Controller
public class RegisterController {

	@Autowired
	private UserService userService;

	@RequestMapping(value={"/register"}, method = RequestMethod.POST)
	public ModelAndView processRegistration(@RequestParam(value = "password2") String password2, @Valid @ModelAttribute("userForm") User user, BindingResult bindingResult, ModelAndView modelAndView){
//		if(user.getEmail().equals("") || user.getPassword().equals("") || user.getUsername().equals("") || user.getSex().equals("NONE")){
//			modelAndView.addObject("error", "Email, sex, username and password are obligatory fields");
//			modelAndView.setViewName("register");
//			return viewRegistration(modelAndView, user);
//		}
		if(userService.getUserByEmail(user.getEmail()) != null){
			modelAndView.addObject("error", "Email already registered");
			modelAndView.setViewName("register");
			return viewRegistration(modelAndView, user);
		}
		if(userService.getUserByUsername(user.getUsername()) != null){
			modelAndView.addObject("error", "User exists");
			modelAndView.setViewName("register");
			return viewRegistration(modelAndView, user);
		}
		String password = user.getPassword();
		if (!password.equals(password2)){// || password == null || password.length() < 4 ) {
			modelAndView.addObject("error", "Passwords do not match");
			modelAndView.setViewName("register");
			return viewRegistration(modelAndView, user);
		}
		if(bindingResult.hasErrors()){
			for (Object object : bindingResult.getAllErrors()) {
			    if(object instanceof FieldError) {
			        FieldError fieldError = (FieldError) object;
//					ObjectError error = bindingResult.getAllErrors().get(0);
					modelAndView.addObject("error", fieldError.getField() + ": " + fieldError.getDefaultMessage());
					modelAndView.setViewName("register");
					return viewRegistration(modelAndView, user);
			    }
			}
		}
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		String encryptedPassword = bCryptPasswordEncoder.encode(password);
		user.setPassword(encryptedPassword);
		userService.saveOrUpdate(user);
		modelAndView.addObject("message", "You've been successfuly registered");
		modelAndView.setViewName("login");
		return modelAndView;
	}

	@RequestMapping(value={"/register"}, method = RequestMethod.GET)
	public ModelAndView viewRegistration(ModelAndView modelAndView, @ModelAttribute("userForm") User user){
		modelAndView.addObject("userForm", user);
		user.setPassword("");
		Map<Constants.Sex,String> sexList = new LinkedHashMap<Constants.Sex,String>();
	    sexList.put(Constants.Sex.FEMALE, "female");
	    sexList.put(Constants.Sex.MALE, "male");
	    modelAndView.addObject("sexList", sexList);
		return modelAndView;

	}
}
