package risto.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import risto.data.model.geoObject.Comment;
import risto.data.model.geoObject.GeoObject;
import risto.data.model.user.User;
import risto.data.model.user.UserRef;
import risto.data.service.GeoObjectService;
import risto.data.service.UserService;

@Controller
public class ObjectDetailsController {

	@Autowired
	private UserService userService;

	@Autowired
	private GeoObjectService geoObjectService;

	@RequestMapping(value={"/protected/addcomment"}, method = RequestMethod.POST)
	public ModelAndView addComment(ModelAndView modelAndView, @ModelAttribute("newcomment") Comment comment, @RequestParam(value = "objectid") String objectId){
		if(comment.getBody().length() > 0){
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			String name = auth.getName();
			User user = userService.getUserByUsername(name);
			comment.setObjectId(objectId);
		    comment.setAuthor(new UserRef(user));
			geoObjectService.addComment(comment, null);
		}
		return new ModelAndView("redirect:/protected/objectdetails?objectid=" + objectId);
	}

	@RequestMapping(value={"/protected/objectdetails"}, method = RequestMethod.GET)
	public ModelAndView objectDetails(@RequestParam(value = "objectid") String objectId, ModelAndView modelAndView){
		GeoObject geoObject = geoObjectService.getGeoObjectById(objectId);
		List<Comment> comments = geoObjectService.getCommentsForGeoObjectId(objectId);
		modelAndView.addObject("commentList", comments);
		modelAndView.addObject("geoObject", geoObject);
		Comment comment = new Comment();
		modelAndView.addObject("newcomment", comment);
		return modelAndView;
	}

}
