package risto.web.controller.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.RememberMeAuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import risto.config.security.RestTokenBasedRememberMeService;
import risto.data.model.user.User;
import risto.data.service.GeoObjectService;
import risto.data.service.UserService;
import risto.misc.Constants.AccessRole;
import risto.web.misc.JsonResponse;

//good article https://stormpath.com/blog/put-or-post/

@RestController
public class MainRestController {

	@Autowired
	private UserService userService;

	@Autowired
	private GeoObjectService geoObjectService;

	@Autowired
	RestTokenBasedRememberMeService tokenBasedRememberMeService;

	@Autowired
	@Qualifier("restAuthenticationManager")
	AuthenticationManager authenticationManager;

	RememberMeAuthenticationProvider rememberMeAuthenticationProvider;

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public @ResponseBody String createEmployee() {
		return "{\"answer\":\"test passed\"}";
	}

	@RequestMapping(value = "/restlogin", method = RequestMethod.GET)
	public ResponseEntity<Object> login(
			HttpServletRequest request, HttpServletResponse response) {
		String username = request.getHeader("X-RST-Username");
		String password = request.getHeader("X-RST-Password");
		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
				username, password);
		try {
			Authentication auth = authenticationManager.authenticate(token);
			tokenBasedRememberMeService.loginSuccess(request, response, auth);
			String name = auth.getName();
			User user = userService.getUserByUsername(name);
			if(user.getRole() == AccessRole.ROLE_ADMIN){
				return new ResponseEntity<Object>(new JsonResponse("admin", tokenBasedRememberMeService.getCurrentTokenValue()), HttpStatus.OK);
			}

		} catch (BadCredentialsException ex) {
			return new ResponseEntity<Object>(new JsonResponse(false, "Wrong username or password"), HttpStatus.FORBIDDEN);
		}
		return new ResponseEntity<Object>(new JsonResponse(true, tokenBasedRememberMeService.getCurrentTokenValue()), HttpStatus.OK);
	}

	@RequestMapping(value = "/restlogout", method = RequestMethod.GET)
	public ResponseEntity<Object> logout(
			HttpServletRequest request, HttpServletResponse response) {
		if(request.getHeader(tokenBasedRememberMeService.getHeaderTokenName()) == null){
			return new ResponseEntity<Object>(new JsonResponse(false, "Please provide token"), HttpStatus.NOT_ACCEPTABLE);
		}
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		try {
			tokenBasedRememberMeService.logout(request, response, auth);
		} catch (BadCredentialsException ex) {
			return new ResponseEntity<Object>(new JsonResponse(false, "Error occured"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Object>(new JsonResponse(true, "logged out"), HttpStatus.OK);
	}

	@RequestMapping(value = "/restgloballogout", method = RequestMethod.GET)
	public ResponseEntity<Object> globalLogout(
			HttpServletRequest request, HttpServletResponse response) {
		String username = request.getHeader("X-RST-Username");
		String password = request.getHeader("X-RST-Password");
		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
				username, password);
		try {
			Authentication auth = authenticationManager.authenticate(token);
			tokenBasedRememberMeService.logoutFromEverywhere(request, response, auth);
		} catch (BadCredentialsException ex) {
			return new ResponseEntity<Object>(new JsonResponse(false, "Wrong username or password"), HttpStatus.FORBIDDEN);
		}
		return new ResponseEntity<Object>(new JsonResponse(true, "logged out"), HttpStatus.OK);
	}

	@RequestMapping(value={"/restregister"}, method = RequestMethod.POST)
	public ResponseEntity<Object> processRegistration(@RequestBody User user, HttpServletRequest httpServletRequest){
		try{
			if(user.getEmail().equals("") || user.getPassword().equals("") || user.getUsername().equals("") || user.getSex().equals("NONE")){
				return new ResponseEntity<Object>(new JsonResponse(false, "Email, sex, username and password are obligatory fields"), HttpStatus.NOT_ACCEPTABLE);
			}
			if(userService.getUserByEmail(user.getEmail()) != null){
				return new ResponseEntity<Object>(new JsonResponse(false, "Email already registered"), HttpStatus.NOT_ACCEPTABLE);
			}
			if(userService.getUserByUsername(user.getUsername()) != null){
				return new ResponseEntity<Object>(new JsonResponse(false, "Username already registered"), HttpStatus.NOT_ACCEPTABLE);
			}
			String password = user.getPassword();
			BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
			String encryptedPassword = bCryptPasswordEncoder.encode(password);
			user.setPassword(encryptedPassword);
			userService.saveOrUpdate(user);
			return new ResponseEntity<Object>(new JsonResponse(true, user.getId()), HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error ocurred"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
