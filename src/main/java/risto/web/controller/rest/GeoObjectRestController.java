package risto.web.controller.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import risto.data.model.GeoArea;
import risto.data.model.Position;
import risto.data.model.geoObject.Comment;
import risto.data.model.geoObject.GeoObject;
import risto.data.model.news.NewsItem;
import risto.data.model.user.User;
import risto.data.model.user.UserRef;
import risto.data.service.GeoObjectService;
import risto.data.service.UserService;
import risto.misc.Constants.AccessRole;
import risto.web.misc.JsonResponse;

//good article https://stormpath.com/blog/put-or-post/

@RestController
public class GeoObjectRestController {

//	@Autowired
	private ObjectMapper objectMapper = new ObjectMapper();

	@Autowired
	private UserService userService;

	@Autowired
	private GeoObjectService geoObjectService;

//	@Autowired
//	@Qualifier("restAuthenticationManager")
//	AuthenticationManager authenticationManager;

//	RememberMeAuthenticationProvider rememberMeAuthenticationProvider;

	
	/**
	 * Add new object
	 */
	@RequestMapping(value = { "/service/objects" }, method = RequestMethod.POST)
	public ResponseEntity<Object> addGeoObject(@Valid @RequestBody GeoObject geoObject, BindingResult bindingResult, HttpServletRequest httpServletRequest) {
		try{
			if (geoObject.getTitle().equals("") || geoObject.getType().equals("") || geoObject.getPosition() == null || geoObject.getPosition().getGps() == null
					|| geoObject.getPosition().getGps().getLat() == null
					|| geoObject.getPosition().getGps().getLon() == null) {
				return new ResponseEntity<Object>(new JsonResponse(false, "title, type and position are mandatory fields") ,HttpStatus.NOT_ACCEPTABLE);
			}
			if(geoObject.getId() != null || geoObject.getAuthor() != null || geoObject.getRealImagePath() != null || geoObject.getPhotoPathes() != null || 
					geoObject.getRating() != 0 || geoObject.getVotes() != null || geoObject.getCommentsCount() != 0){
				return new ResponseEntity<Object>(new JsonResponse(false, "id, author, image, photos, rating, votes and commentsCount cannot be set"), HttpStatus.NOT_ACCEPTABLE);
			}
			geoObject.setDate(new Date());
			if(bindingResult.hasErrors()){
				for (Object object : bindingResult.getAllErrors()) {
				    if(object instanceof FieldError) {
				        FieldError fieldError = (FieldError) object;
						return new ResponseEntity<Object>(new JsonResponse(false, fieldError.getField() + ": " + fieldError.getDefaultMessage()), HttpStatus.NOT_ACCEPTABLE);
				    }
				}
			}
			Authentication auth = SecurityContextHolder.getContext()
					.getAuthentication();
			String name = auth.getName();
			User user = userService.getUserByUsername(name);
			geoObject.setAuthor(new UserRef(user));
			geoObjectService.add(geoObject);
			return new ResponseEntity<Object>(new JsonResponse(true, geoObject.getId()), HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error occured"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = { "/service/objects/id/{id}" }, method = RequestMethod.PUT)
	public ResponseEntity<Object> editGeoObjectPost(@PathVariable String id, HttpServletRequest request) {
		try {
			GeoObject geoObject = geoObjectService.getGeoObjectById(id);
			if(geoObject == null){
				return new ResponseEntity<Object>(new JsonResponse(false, "object not found"), HttpStatus.NOT_FOUND);
			}
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			String name = auth.getName();
			User user = userService.getUserByUsername(name);
			if(!user.getId().equals(geoObject.getAuthor().getId()) && user.getRole() != AccessRole.ROLE_ADMIN){
				return new ResponseEntity<Object>(new JsonResponse(false, "only author can update the object"), HttpStatus.FORBIDDEN);
			}
			GeoObject updatedGeoObject;
			updatedGeoObject = objectMapper.readerForUpdating(geoObject).readValue(request.getReader());
			geoObjectService.update(updatedGeoObject);
			return new ResponseEntity<Object>(new JsonResponse(true, "edited"),HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error occured"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = { "/service/objects/id/{id}" }, method = RequestMethod.DELETE)
	public ResponseEntity<Object> deleteGeoObjectPost(@PathVariable String id, HttpServletRequest request) {
		try {
			GeoObject geoObject = geoObjectService.getGeoObjectById(id);
			if(geoObject == null){
				return new ResponseEntity<Object>(new JsonResponse(false, "object not found"), HttpStatus.NOT_FOUND);
			}
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			String name = auth.getName();
			User user = userService.getUserByUsername(name);
			if(!user.getId().equals(geoObject.getAuthor().getId()) && user.getRole() != AccessRole.ROLE_ADMIN){
				return new ResponseEntity<Object>(new JsonResponse(false, "only author can delete the object"), HttpStatus.FORBIDDEN);
			}
			geoObjectService.deleteObject(geoObject);
			return new ResponseEntity<Object>(new JsonResponse(true, "deleted"),HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error occured"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = {"/service/objects/nearby"}, method = RequestMethod.GET)
	public ResponseEntity<Object> nearbyObjects(@RequestParam(value = "lon") String lon, @RequestParam(value = "lat") String lat, @RequestParam(value = "dst") String distance) {
		try{
			List<GeoObject> geoObjects = geoObjectService.getGeoObjectsInRange(new Position(Double.parseDouble(lon), Double.parseDouble(lat)), Double.parseDouble(distance));
			return new ResponseEntity<Object>(new JsonResponse(true, geoObjects), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error ocurred"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

//	@SuppressWarnings("serial")
	static class StringList extends ArrayList<String> {
	    private static final long serialVersionUID = 1L;
	    public StringList() { super(); }
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = {"/service/objects/nearbyandpopulateexternal"}, method = RequestMethod.POST)
	public ResponseEntity<Object> nearbyObjectsAndPopulate(@RequestBody Map objectsToPopulate, @RequestParam(value = "lon") String lon, @RequestParam(value = "lat") String lat, @RequestParam(value = "dst") String distance) {
		try{
			String value = objectsToPopulate.values().toString();
			value = value.replace("[", "");
			value = value.replace("]", "");
			value = value.replace(" ", "");
			ArrayList<String> values = new ArrayList<String>(Arrays.asList(value.split(",")));
			for (int i = 0; i < values.size(); i++) {
				values.set(i, "'" + values.get(i) + "'");
			}
			List<GeoObject> geoObjects = geoObjectService.getGeoObjectsInRange(new Position(Double.parseDouble(lon), Double.parseDouble(lat)), Double.parseDouble(distance));
			List<GeoObject> externalObjects = geoObjectService.getOblectsForLinkedIds(values);
			geoObjects.addAll(externalObjects);
			return new ResponseEntity<Object>(new JsonResponse(true, geoObjects), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error ocurred"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = {"/service/objects/populateexternal"}, method = RequestMethod.POST)
	public ResponseEntity<Object> populate(@RequestBody Map objectsToPopulate, @RequestParam(value = "lon") String lon, @RequestParam(value = "lat") String lat, @RequestParam(value = "dst") String distance) {
		try{
			String value = objectsToPopulate.values().toString();
			value = value.replace("[", "");
			value = value.replace("]", "");
			value = value.replace(" ", "");
			ArrayList<String> values = new ArrayList<String>(Arrays.asList(value.split(",")));
			for (int i = 0; i < values.size(); i++) {
				values.set(i, "'" + values.get(i) + "'");
			}
//			List<GeoObject> geoObjects = geoObjectService.getGeoObjectsInRange(new Position(Double.parseDouble(lon), Double.parseDouble(lat)), Double.parseDouble(distance));
			List<GeoObject> externalObjects = geoObjectService.getOblectsForLinkedIds(values);
//			geoObjects.addAll(externalObjects);
			return new ResponseEntity<Object>(new JsonResponse(true, externalObjects), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error ocurred"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = {"/service/objects/types"}, method = RequestMethod.GET)
	public ResponseEntity<Object> objectTypes() {
		try{
			List<String> types = geoObjectService.getObjectTypes();
			return new ResponseEntity<Object>(new JsonResponse(true, types), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error ocurred"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value={"/service/objects/id/{id}/comments"}, method = RequestMethod.GET)
	public ResponseEntity<Object> listComments(@PathVariable String id,
			@RequestParam(value="provider_type", required=false) String providerType){
		try{
			if(providerType!= null && !providerType.equals("SELF")){
				GeoObject geoObject = geoObjectService.getGeoObjectByLinkedObjectId(id);
				if(geoObject != null){
					id = geoObject.getId();
				}else{
					return new ResponseEntity<Object>(new JsonResponse(true, new ArrayList<Comment>()), HttpStatus.OK);
				}
			}
			List<Comment> comments = geoObjectService.getCommentsForGeoObjectId(id);
			return new ResponseEntity<Object>(new JsonResponse(true, comments), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error ocurred"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value={"/service/objects/id/{id}/comments"}, method = RequestMethod.POST)
	public ResponseEntity<Object> addComment(@RequestBody Comment comment, @PathVariable String id,
			@RequestParam(value="parent_id", required=false) String parentId,
			@RequestParam(value="provider_type", required=false) String providerType,
			@RequestParam(value="object_title", required=false) String objectTitle,
			@RequestParam(value="object_lat", required=false) String objectLat,
			@RequestParam(value="object_lon", required=false) String objectLon,
			HttpServletRequest httpServletRequest){
		try{
			if (comment.getBody() == null || comment.getBody().length() == 0) {
				return new ResponseEntity<Object>(new JsonResponse(false, "comment body cannot be empty") ,HttpStatus.NOT_ACCEPTABLE);
			}
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			String name = auth.getName();
			User user = userService.getUserByUsername(name);
			comment.setAuthor(new UserRef(user));
			GeoObject geoObject;
			if(providerType!= null && !providerType.equals("SELF")){
				geoObject = geoObjectService.getGeoObjectByLinkedObjectId(id);
				if(geoObject == null){
					geoObject = new GeoObject();
					geoObject.setLinkedObjectId(id);
					geoObject.setType("PLACE");
					geoObject.setObjectProvider(providerType);
					geoObjectService.add(geoObject);
				}
				if(objectLon != null && objectLat != null)
					geoObject.setPosition(new Position(Double.parseDouble(objectLon), Double.parseDouble(objectLat)));
			}else{
				geoObject = geoObjectService.getGeoObjectById(id);
			}
			comment.setObjectId(geoObject.getId());
			geoObjectService.addComment(comment, parentId, objectTitle);
			return new ResponseEntity<Object>(new JsonResponse(true, comment), HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error ocurred"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = {"/service/objects/id/{id}"}, method = RequestMethod.GET)
	public ResponseEntity<Object> objectDetails(@PathVariable String id){
		try{
			GeoObject geoObject = geoObjectService.getGeoObjectById(id);
			return new ResponseEntity<Object>(new JsonResponse(true, geoObject), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error ocurred"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = {"/service/objects/all"}, method = RequestMethod.GET)
	public ResponseEntity<Object> allObjects(){
		try{
			Authentication auth = SecurityContextHolder.getContext()
					.getAuthentication();
			String name = auth.getName();
			User user = userService.getUserByUsername(name);
			if(user.getRole() != AccessRole.ROLE_ADMIN){
				return new ResponseEntity<Object>(new JsonResponse(false, "forbidden"), HttpStatus.FORBIDDEN);
			}
			List<GeoObject> geoObjects = geoObjectService.getAllGeoObjects();
			return new ResponseEntity<Object>(new JsonResponse(true, geoObjects), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error ocurred"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/service/objects/area_objects", method = RequestMethod.GET)
	public ResponseEntity<Object> localFieldObjects(
			@RequestParam(required=false) String countryCode,
			@RequestParam(required=false) String postalCode,
			@RequestParam(required=false) String administrativeArea,
			@RequestParam(required=false) String subAdministrativeArea,
			@RequestParam(required=false) String locality,
			@RequestParam(required=false) String subLocality,
			@RequestParam(required=false) String thoroughfare,
			@RequestParam(required=false) String subThoroughfare
			) {
		try {
			Position position = new Position();
			GeoArea geoArea = new GeoArea();
			position.setGeoArea(geoArea);
			geoArea.setCountryCode(countryCode);
			geoArea.setPostalCode(postalCode);
			geoArea.setAdministrativeArea(administrativeArea);
			geoArea.setSubAdministrativeArea(subAdministrativeArea);
			geoArea.setLocality(locality);
			geoArea.setSubLocality(subLocality);
			geoArea.setThoroughfare(thoroughfare);
			geoArea.setSubThoroughfare(subThoroughfare);
			
			List<GeoObject> news = geoObjectService.getGeoObjectsForPosition(position);
			return new ResponseEntity<Object>(new JsonResponse(true, news), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error ocurred"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
