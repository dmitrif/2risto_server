package risto.web.controller.rest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.RememberMeAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import risto.config.security.RestTokenBasedRememberMeService;
import risto.data.model.GeoArea;
import risto.data.model.Position;
import risto.data.model.geoObject.GeoObject;
import risto.data.model.news.NewsItem;
import risto.data.model.user.Message;
import risto.data.model.user.User;
import risto.data.model.user.UserRef;
import risto.data.service.GeoObjectService;
import risto.data.service.UserService;
import risto.misc.Constants.AccessRole;
import risto.web.misc.JsonResponse;

//good article https://stormpath.com/blog/put-or-post/

@RestController
public class UserRestController {

//	@Autowired
//	private ObjectMapper objectMapper = new ObjectMapper();

	@Autowired
	private UserService userService;

	@Autowired
	private GeoObjectService geoObjectService;

	@Autowired
	RestTokenBasedRememberMeService tokenBasedRememberMeService;

	@Autowired
	@Qualifier("restAuthenticationManager")
	AuthenticationManager authenticationManager;

	RememberMeAuthenticationProvider rememberMeAuthenticationProvider;

	@RequestMapping(value = "/service/users/username/{username}", method = RequestMethod.GET)
	public ResponseEntity<Object> userInfoByUsername(@PathVariable String username) {
		try {
			User user = userService.getUserByUsername(username);
			if(user == null){
				return new ResponseEntity<Object>(new JsonResponse(false, "user does not exists"), HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<Object>(new JsonResponse(true, user), HttpStatus.OK);
		} catch (Exception e) {
//			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error occured"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/service/users/id/{id}", method = RequestMethod.GET)
	public ResponseEntity<Object> userInfoById(@PathVariable String id) {
		try {
			User user = userService.getUserById(id);
			if(user == null){
				return new ResponseEntity<Object>(new JsonResponse(false, "user does not exists"), HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<Object>(new JsonResponse(true, user), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(new JsonResponse(false, "error occured"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = {"/service/users/nearby"}, method = RequestMethod.GET)
	public ResponseEntity<Object> nearbyUsers(@RequestParam(value = "lon") String lon, @RequestParam(value = "lat") String lat, @RequestParam(value = "dst") String distance) {
		try {
			List<User> users = userService.getUsersInRange(new Position(Double.parseDouble(lon), Double.parseDouble(lat)), Double.parseDouble(distance));
			return new ResponseEntity<Object>(new JsonResponse(true, users), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error ocurred"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/service/users/id/{id}/objects", method = RequestMethod.GET)
	public ResponseEntity<Object> listObjectsForUser(@PathVariable String id) {
		try {
			List<GeoObject> geoObjects = userService.getGeoObjectsForUserId(id);
			return new ResponseEntity<Object>(new JsonResponse(true, geoObjects), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error ocurred"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = {"/service/objects/id/{id}/follow"}, method = RequestMethod.PUT)
	public ResponseEntity<Object> followObject(@PathVariable String id){
		try{
			GeoObject geoObject = geoObjectService.getGeoObjectById(id);
			if(geoObject == null){
				return new ResponseEntity<Object>(new JsonResponse(false, "object not found"), HttpStatus.NOT_FOUND);
			}
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			String name = auth.getName();
			User user = userService.getUserByUsername(name);
			userService.followObject(user, geoObject);
			return new ResponseEntity<Object>(new JsonResponse(true, "followed"), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error ocurred"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = {"/service/users/id/{id}/follow"}, method = RequestMethod.PUT)
	public ResponseEntity<Object> followUser(@PathVariable String id){
		try{
			User fUser = userService.getUserById(id);
			if(fUser == null){
				return new ResponseEntity<Object>(new JsonResponse(false, "user not found"), HttpStatus.NOT_FOUND);
			}
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			String name = auth.getName();
			User user = userService.getUserByUsername(name);
			userService.followUser(user, fUser);
			return new ResponseEntity<Object>(new JsonResponse(true, "followed"), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error ocurred"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = {"/service/objects/id/{id}/unfollow"}, method = RequestMethod.PUT)
	public ResponseEntity<Object> unfollowObject(@PathVariable String id){
		try{
			GeoObject geoObject = geoObjectService.getGeoObjectById(id);
			if(geoObject == null){
				return new ResponseEntity<Object>(new JsonResponse(false, "object not found"), HttpStatus.NOT_FOUND);
			}
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			String name = auth.getName();
			User user = userService.getUserByUsername(name);
			userService.unfollowObject(user, geoObject);
			return new ResponseEntity<Object>(new JsonResponse(true, "followed"), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error ocurred"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = {"/service/users/id/{id}/unfollow"}, method = RequestMethod.PUT)
	public ResponseEntity<Object> unfollowUser(@PathVariable String id){
		try{
			User fUser = userService.getUserById(id);
			if(fUser == null){
				return new ResponseEntity<Object>(new JsonResponse(false, "user not found"), HttpStatus.NOT_FOUND);
			}
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			String name = auth.getName();
			User user = userService.getUserByUsername(name);
			userService.unfollowUser(user, fUser);
			return new ResponseEntity<Object>(new JsonResponse(true, "unfollowed"), HttpStatus.OK);
		} catch (Exception e) {
//			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error ocurred"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value={"/service/messages"}, method = RequestMethod.POST)
	public ResponseEntity<Object> addMessage(@RequestBody Message message, HttpServletRequest httpServletRequest){
		try{
			if (message.getBody() == null || message.getBody().length() == 0) {
				return new ResponseEntity<Object>(new JsonResponse(false, "message cannot be empty") ,HttpStatus.NOT_ACCEPTABLE);
			}
			if(message.getTo() == null || message.getTo().size() == 0){
				return new ResponseEntity<Object>(new JsonResponse(false, "please add at least one recipient") ,HttpStatus.NOT_ACCEPTABLE);
			}
			if (message.getTo() == null || message.getBody().length() == 0) {
				return new ResponseEntity<Object>(new JsonResponse(false, "message cannot be empty") ,HttpStatus.NOT_ACCEPTABLE);
			}
			for(UserRef user : message.getTo()){
				if(user.getId() == null && user.getUsername() == null)
					return new ResponseEntity<Object>(new JsonResponse(false, "one of userId or username are required") ,HttpStatus.NOT_ACCEPTABLE);					
				if(user.getUsername() == null)
					user.setUsername(userService.getUserById(user.getId()).getUsername());
				else
					user.setId(userService.getUserByUsername(user.getUsername()).getId());
			}			
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			String name = auth.getName();
			User user = userService.getUserByUsername(name);
			message.setFrom(new UserRef(user));
			userService.add(message);
			return new ResponseEntity<Object>(new JsonResponse(true, message), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<Object>(new JsonResponse(false, "error ocurred"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/service/messages/sent", method = RequestMethod.GET)
	public ResponseEntity<Object> messagesFromUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		User user = userService.getUserByUsername(name);
		List<Message> messages = userService.getMessagesFromUser(user);
		return new ResponseEntity<Object>(new JsonResponse(true, messages), HttpStatus.OK);
	}

	@RequestMapping(value = "/service/news/followed", method = RequestMethod.GET)
	public ResponseEntity<Object> followeesNews() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		User user = userService.getUserByUsername(name);
		List<NewsItem> news = userService.getNewsForUser(user);
		return new ResponseEntity<Object>(new JsonResponse(true, news), HttpStatus.OK);
	}

	@RequestMapping(value = "/service/news/nearby", method = RequestMethod.GET)
	public ResponseEntity<Object> nearbyNews(@RequestParam(value = "lon") String lon, @RequestParam(value = "lat") String lat, @RequestParam(value = "dst") String distance) {
		try {
			List<NewsItem> news = userService.getNewsInRange(new Position(Double.parseDouble(lon), Double.parseDouble(lat)), Double.parseDouble(distance));
			return new ResponseEntity<Object>(new JsonResponse(true, news), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error ocurred"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/service/news/all", method = RequestMethod.GET)
	public ResponseEntity<Object> allNews() {
		try {
			List<NewsItem> news = userService.getAllNews();
			return new ResponseEntity<Object>(new JsonResponse(true, news), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error ocurred"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/service/news/area_news", method = RequestMethod.GET)
	public ResponseEntity<Object> localFieldNews(
			@RequestParam(required=false) String countryCode,
			@RequestParam(required=false) String postalCode,
			@RequestParam(required=false) String administrativeArea,
			@RequestParam(required=false) String subAdministrativeArea,
			@RequestParam(required=false) String locality,
			@RequestParam(required=false) String subLocality,
			@RequestParam(required=false) String thoroughfare,
			@RequestParam(required=false) String subThoroughfare
			) {
		try {
			Position position = new Position();
			GeoArea geoArea = new GeoArea();
			position.setGeoArea(geoArea);
			geoArea.setCountryCode(countryCode);
			geoArea.setPostalCode(postalCode);
			geoArea.setAdministrativeArea(administrativeArea);
			geoArea.setSubAdministrativeArea(subAdministrativeArea);
			geoArea.setLocality(locality);
			geoArea.setSubLocality(subLocality);
			geoArea.setThoroughfare(thoroughfare);
			geoArea.setSubThoroughfare(subThoroughfare);
			
			List<NewsItem> news = userService.getNewsForPosition(position);
			return new ResponseEntity<Object>(new JsonResponse(true, news), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error ocurred"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/service/messages/all", method = RequestMethod.GET)
	public ResponseEntity<Object> allMessagesWithUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		User user = userService.getUserByUsername(name);
		List<Message> messages = userService.getMessagesWithUser(user);
		return new ResponseEntity<Object>(new JsonResponse(true, messages), HttpStatus.OK);
	}

	@RequestMapping(value = "/service/messages", method = RequestMethod.GET)
	public ResponseEntity<Object> messagesToUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		User user = userService.getUserByUsername(name);
		List<Message> messages = userService.getMessagesToUser(user);
		return new ResponseEntity<Object>(new JsonResponse(true, messages), HttpStatus.OK);
	}

	@RequestMapping(value = "/service/messages/id/{id}/seen", method = RequestMethod.PUT)
	public ResponseEntity<Object> setMessageSeen(@PathVariable String id) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		User user = userService.getUserByUsername(name);
		Message message = userService.getMessage(id);
		if(!message.getFrom().equals(new UserRef(user)) && user.getRole() != AccessRole.ROLE_ADMIN){
			return new ResponseEntity<Object>(new JsonResponse(false, "only author can change message status"), HttpStatus.FORBIDDEN);
		}
		message.setSeen(true);
		userService.update(message);
		return new ResponseEntity<Object>(new JsonResponse(true, "set"), HttpStatus.OK);
	}

	@RequestMapping(value = "/service/users/checkin", method = RequestMethod.PUT)
	public ResponseEntity<Object> checkinUser(@RequestParam String objectId,
			@RequestParam(value="provider_type", required=false) String providerType) {
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			String name = auth.getName();
			User user = userService.getUserByUsername(name);
			GeoObject geoObject;
			if(providerType!= null && !providerType.equals("SELF")){
				geoObject = geoObjectService.getGeoObjectByLinkedObjectId(objectId);
				if(geoObject == null){
					geoObject = new GeoObject();
					geoObject.setLinkedObjectId(objectId);
					geoObject.setType("PLACE");
					geoObject.setObjectProvider(providerType);
					geoObjectService.add(geoObject);
				}
			}else{
				geoObject = geoObjectService.getGeoObjectById(objectId);
			}
			String ret = userService.checkinUserAtObject(user, geoObject.getId());
			if(ret == null)
				return new ResponseEntity<Object>(new JsonResponse(false, "already checked in"), HttpStatus.ALREADY_REPORTED);
			return new ResponseEntity<Object>(new JsonResponse(true, "checked in"), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(new JsonResponse(false, "error ocurred"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}