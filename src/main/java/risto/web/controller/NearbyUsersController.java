package risto.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import risto.data.model.Position;
import risto.data.model.user.User;
import risto.data.service.UserService;

@Controller
public class NearbyUsersController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = {"protected/nearbyusers"}, method = RequestMethod.GET)
	public ModelAndView homePage(@RequestParam(value = "lon") String lon, @RequestParam(value = "lat") String lat, @RequestParam(value = "dst") String distance) {
		List<User> geoObjects = userService.getUsersInRange(new Position(Double.parseDouble(lon), Double.parseDouble(lat)), Double.parseDouble(distance));
		ModelAndView model = new ModelAndView();
		model.addObject("userList", geoObjects);
		model.setViewName("protected/listusers");
		return model;
	}
}
