package risto.web.misc;

public class JsonResponse {

	private String status;

	private Object response;
	
    public JsonResponse(boolean isSuccess, Object body) {
    	if(isSuccess)
    		this.setStatus("success");
    	else
    		this.setStatus("error");
		this.setBody(body);
    }

    public JsonResponse(String status, Object body) {
   		this.setStatus(status);
		this.setBody(body);
    }

    public JsonResponse() {
    }

	public Object getBody() {
		return response;
	}

	public void setBody(Object body) {
		this.response = body;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}