package risto.misc;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.stereotype.Service;

@Service
public class UtilityService {

	private final static String from = "risto@7779.me";
	private final static String host = "7779me.ipage.com";
	private final static String password = "Qwerty1";

	public void sendEmail(String to, String subject, String body) {
		Properties properties = System.getProperties();
		Session session = Session.getInstance(properties);
		try {
			body = StringEscapeUtils.unescapeHtml4(body);
			subject = StringEscapeUtils.unescapeHtml4(subject);
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					to));
			message.setSubject(subject, "utf-8");
			message.setHeader("Content-Type", "text/plain; charset=UTF-8");
			message.setText(body, "utf-8");
			final Transport transport = session.getTransport("smtp");
			transport.connect(host, 587, from, password);
			transport.sendMessage(message, message.getAllRecipients());
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}
}
