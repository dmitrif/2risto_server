package risto.misc;

import java.util.HashMap;


public class Constants {
	public enum News {
	    NEW_OBJECT, NEW_COMMENT, NEW_CHEKIN;
	}

	public enum Sex {
	    MALE, FEMALE;
	}

	public enum AccessRole {
	    ROLE_USER, ROLE_ADMIN, ROLE_GUEST;
	}
	
	private static HashMap<String, String> defaultImageMap;
	static
	{
		defaultImageMap = new HashMap<String, String>();
		defaultImageMap.put("BUZZ", "buzz.png");
		defaultImageMap.put("OBJECT", "object.png");
		defaultImageMap.put("EVENT", "event.png");
		defaultImageMap.put("REQUEST", "request.png");
	};
			
	public final static String DEFAULT_USER_IMAGE_PATH="";
	public final static String DEFAULT_IMAGE_PATH="";
	
	public static String getDefaultImagePathForObjectType(String type){
		String ret = defaultImageMap.get(type);
		if(ret == null)
			return DEFAULT_IMAGE_PATH;
		else
			return ret;
	}
}
