package risto.data.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

import risto.data.model.Position;
import risto.data.model.user.User;
import risto.data.model.user.UserRef;

@Component("userDao")
public class UserDaoImpl extends GenericDaoImpl<User, String> implements UserDao{

	public UserDaoImpl() {
		super(User.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<User> getUsersFromUserRefSet(Set<UserRef> refSet){
		if(refSet.size() == 0)
			return new ArrayList<User>();
		UserRef[] refArr = refSet.toArray(new UserRef[0]);
		String users = "[{$oid:'" + refArr[0].getId() + "'}";
		for(int i = 1; i < refArr.length; i++){// UserRef uRef : refList){
			users += ",{$oid:'" + refArr[i].getId() + "'}";
		}
		users += "]";
		return entityManager.createNativeQuery("{ _id:{$in:" + users + "}}", User.class).getResultList();		
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<User> getUsersInRange(Position position, Double distance){
		String query = "{ position.gps : { $near : " + position.toString() + " , $maxDistance : " + distance + "  } }";
		List<User> res = entityManager.createNativeQuery(query, User.class).getResultList();		
		return res;
	}

	@Override
	public User getUserByEmail(String email) {
		List<User> res = getList("email", email);
		if(res.size() > 0)
			return res.get(0);
		else
			return null;
	}

	@Override
	public User getUserByUsername(String username) {
		List<User> res = getList("username", username);
		if(res.size() > 0)
			return res.get(0);
		else
			return null;
	}
}