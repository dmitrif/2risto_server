package risto.data.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

import risto.data.model.GeoArea;
import risto.data.model.Position;
import risto.data.model.geoObject.GeoObject;
import risto.data.model.geoObject.GeoObjectRef;
import risto.data.model.news.NewsItem;
import risto.data.model.user.User;
import risto.data.model.user.UserRef;

@Component("newsDao")
public class NewsDaoImpl extends GenericDaoImpl<NewsItem, String> implements NewsDao{

	public NewsDaoImpl() {
		super(NewsItem.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<NewsItem> getNewsForUser(User user){
		if(user.getfUsers().size() == 0 && user.getfObjects().size() == 0)
			return new ArrayList<NewsItem>();
		Set<UserRef> fUsers = user.getfUsers();
		Set<GeoObjectRef> fObjects = user.getfObjects();
		String users = "[]";
		if(user.getfUsers().size() > 0){
			UserRef[] userRefArr = fUsers.toArray(new UserRef[0]);
			users = "[{$oid:'" + userRefArr[0].getId() + "'}";
			for(int i = 1; i < userRefArr.length; i++){// UserRef uRef : refList){
				users += ",{$oid:'" + userRefArr[i].getId() + "'}";
			}
			users += "]";
		}
		String objects = "[]";
		if(user.getfObjects().size() > 0){
			GeoObjectRef[] objRefArr = fObjects.toArray(new GeoObjectRef[0]);
			objects = "[{$oid:'" + objRefArr[0].getId() + "'}";
			for(int i = 1; i < objRefArr.length; i++){// UserRef uRef : refList){
				objects += ",{$oid:'" + objRefArr[i].getId() + "'}";
			}
			objects += "]";
		}
		return entityManager.createNativeQuery("{$or:[{ author.id:{$in:" + users + "}},{ object.id:{$in:" + objects + "}}]}", NewsItem.class).getResultList();		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<NewsItem> getNewsInRange(Position position, Double distance){
		String query = "{ position.gps : { $near : " + position.toString() + " , $maxDistance : " + distance + "  } }";
		List<NewsItem> res = entityManager.createNativeQuery(query, NewsItem.class).getResultList();		
		return res;
	}

	@Override
	public List<NewsItem> getAllNews(){
		return super.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<NewsItem> getNewsForPosition(Position position){
		if(position.getGeoArea() == null)
			return new ArrayList<NewsItem>();
		GeoArea geoArea = position.getGeoArea();
		String andString = "";
		if(geoArea.getCountryCode() != null){
			andString += "{ position.geoArea.countryCode : '" + geoArea.getCountryCode() + "'},";
		}
		if(geoArea.getPostalCode() != null){
			andString += "{ position.geoArea.postalCode : '" + geoArea.getPostalCode() + "'},";
		}
		if(geoArea.getAdministrativeArea() != null){
			andString += "{ position.geoArea.administrativeArea : '" + geoArea.getAdministrativeArea() + "'},";
		}
		if(geoArea.getSubAdministrativeArea() != null){
			andString += "{ position.geoArea.subAdministrativeArea : '" + geoArea.getSubAdministrativeArea() + "'},";
		}
		if(geoArea.getLocality() != null){
			andString += "{ position.geoArea.locality : '" + geoArea.getLocality() + "'},";
		}
		if(geoArea.getSubLocality() != null){
			andString += "{ position.geoArea.subLocality : '" + geoArea.getSubLocality() + "'},";
		}
		if(geoArea.getThoroughfare() != null){
			andString += "{ position.geoArea.thoroughfare : '" + geoArea.getThoroughfare() + "'},";
		}
		if(geoArea.getSubThoroughfare() != null){
			andString += "{ position.geoArea.subThoroughfare : '" + geoArea.getSubThoroughfare() + "'},";
		}
		if(andString.length() == 0)
			return new ArrayList<NewsItem>();
		andString = andString.substring(0, andString.length()-1);
		return entityManager.createNativeQuery("{$and:[ " + andString + " ]}", NewsItem.class).getResultList();		
	}
}