package risto.data.dao;

import java.util.List;
import java.util.Set;

import risto.data.model.Position;
import risto.data.model.user.User;
import risto.data.model.user.UserRef;

public interface UserDao extends GenericDao<User, String>{
	
	public List<User> getUsersInRange(Position position, Double distance);
	public User getUserByEmail(String email);
	public User getUserByUsername(String username);
	public List<User> getUsersFromUserRefSet(Set<UserRef> refSet);
}
