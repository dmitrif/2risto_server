package risto.data.dao;

import java.util.List;
import risto.data.model.Position;
import risto.data.model.news.NewsItem;
import risto.data.model.user.User;

public interface NewsDao extends GenericDao<NewsItem, String>{
	List<NewsItem> getNewsInRange(Position position, Double distance);
	List<NewsItem> getNewsForUser(User user);
	List<NewsItem> getAllNews();
	List<NewsItem> getNewsForPosition(Position position);
}
