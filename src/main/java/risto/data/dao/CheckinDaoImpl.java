package risto.data.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.stereotype.Component;

import risto.data.model.user.Checkin;

@Component("checkinDao")
public class CheckinDaoImpl extends GenericDaoImpl<Checkin, String> implements CheckinDao{

	public CheckinDaoImpl() {
		super(Checkin.class);
	}

	@Override
	public List<Checkin> getCheckinsForUser(String id) {
		return super.getNativeList("\"user_id\"", "{$oid: \"" + id + "\"}");
	}

	@Override
	public boolean isCheckinExists(String userId, String objectId) {
		try{
		return (entityManager.createNativeQuery("{$and:[{ user_id:{$oid: \"" + userId + "\"}},{ object_id:{$oid: \"" + objectId + "\"}}]}"
				, Checkin.class).getSingleResult() != null);
		}
		catch (NoResultException e) {
			return false;
		}
	}
}
