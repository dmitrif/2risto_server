package risto.data.dao;

import java.util.List;

import risto.data.model.geoObject.Comment;

public interface CommentDao extends GenericDao<Comment, String>{

	List<Comment> getCommentsForGeoObjectId(String id);

}
