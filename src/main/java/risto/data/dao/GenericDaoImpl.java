package risto.data.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public abstract class GenericDaoImpl<E, K> implements GenericDao<E, K>{

	@PersistenceContext
	protected EntityManager entityManager;

	Class<E> clazz;

    public GenericDaoImpl(Class<E> clazz) {
        this.clazz = clazz;
    }

	@Override
    public E getById(String id){
    	return entityManager.find(clazz, id);
    }

	@SuppressWarnings("unchecked")
	public List<E> getNativeList(String field, K param) {
		return entityManager.createNativeQuery("{ " + field + " : " + param + " }", clazz).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<E> getList(String field, K param) {
		Query query = entityManager.createQuery("from " + clazz.getName() + " where " + field + "=:param")
		.setParameter("param", param);
		return query.getResultList();
	}
	
	@Override
	@Transactional
	public void add(E entity){
		entityManager.persist(entity);
	}

	@Override
	@Transactional
	public void update(E entity){
		entityManager.merge(entity);
	}

    @SuppressWarnings("unchecked")
	@Override
    public List<E> list() {
		return entityManager.createQuery("from " + clazz.getName()).getResultList();
    }

	@Override
	@Transactional
	public void delete(E entity) {
		entityManager.remove(entity);
	}
}