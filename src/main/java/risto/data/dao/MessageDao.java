package risto.data.dao;

import java.util.List;

import risto.data.model.user.Message;

public interface MessageDao extends GenericDao<Message, String>{

	List<Message> getMessagesToUser(String id);
	List<Message> getMessagesFromUser(String id);
	List<Message> getMessagesWithUser(String id);

}