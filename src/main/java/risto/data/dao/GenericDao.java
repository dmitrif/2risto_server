package risto.data.dao;

import java.util.List;

public interface GenericDao<E, K> {	//E - entity type, K - key

	public List<E> getList(String field, K param);
	public void add(E entity);
	public void update(E entity);
	public List<E> list();
	public void delete(E entity);
	public E getById(String id);

}