package risto.data.dao;

import java.util.List;
import org.springframework.stereotype.Component;
import risto.data.model.geoObject.Comment;

@Component("commentDao")
public class CommentDaoImpl extends GenericDaoImpl<Comment, String> implements CommentDao{

	public CommentDaoImpl() {
		super(Comment.class);
	}

	@Override
	public List<Comment> getCommentsForGeoObjectId(String id) {
		return super.getNativeList("object_id", "{$oid: \"" + id + "\"}");
	}

}
