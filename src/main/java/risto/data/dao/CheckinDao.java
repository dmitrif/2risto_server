package risto.data.dao;

import java.util.List;

import risto.data.model.user.Checkin;

public interface CheckinDao extends GenericDao<Checkin, String>{

	List<Checkin> getCheckinsForUser(String id);
	boolean isCheckinExists(String userId, String objectId);

}