package risto.data.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import risto.data.model.user.Message;

@Component("messageDao")
public class MessageDaoImpl extends GenericDaoImpl<Message, String> implements MessageDao{

	public MessageDaoImpl() {
		super(Message.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Message> getMessagesToUser(String id) {
		return entityManager.createNativeQuery("{ to: { $elemMatch: {id: {$oid:'" + id + "'}}}}", Message.class).getResultList();		
	}

	@Override
	public List<Message> getMessagesFromUser(String id) {
		return super.getNativeList("from.id", "{$oid: \"" + id + "\"}");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Message> getMessagesWithUser(String id) {
		return entityManager.createNativeQuery("{$or:[{ to: { $elemMatch: {id: {$oid:'" + id + "'}}}}, { from.id : {$oid: '" + id + "'} }]}", Message.class).getResultList();		
	}
}
