package risto.data.dao;

import java.util.List;

import risto.data.model.Position;
import risto.data.model.geoObject.GeoObject;
import risto.data.model.news.NewsItem;

public interface GeoObjectDao extends GenericDao<GeoObject, String>{

	public List<GeoObject> getGeoObjectsInRange(String string, Position position, Double distance);
	public List<GeoObject> getAllGeoObjects();
	public List<GeoObject> getGeoObjectsForUserId(String userId);
	public GeoObject getGeoObjectByTitle(String title);
	public List<String> getObjectTypes();
	public List<GeoObject> getGeoObjectsForPosition(Position position);
	public GeoObject getByLinkedObjectId(String id);
	public List<GeoObject> getOblectsForLinkedIds(List<String> objects);
}
