package risto.data.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import risto.data.model.GeoArea;
import risto.data.model.Position;
import risto.data.model.geoObject.GeoObject;
import risto.data.model.geoObject.GeoObjectType;
import risto.data.model.news.NewsItem;

@Component("geoObjectDao")
public class GeoObjectDaoImpl extends GenericDaoImpl<GeoObject, String> implements GeoObjectDao{

	private final String filterSelf = "{$or:[{objectProvider:'SELF'},{ objectProvider: { $exists: false } }]}";
	public GeoObjectDaoImpl() {
		super(GeoObject.class);
	}

	@SuppressWarnings("unchecked")
	public List<GeoObject> getGeoObjectsInRange(String positionField, Position position, Double distance){
		String query = "{ " + positionField + " : { $near : " + position.toString() + " , $maxDistance : " + distance + "  } }";
		query = "{ $and: [" + query + "," + filterSelf + "]}";
		List<GeoObject> res = entityManager.createNativeQuery(query, GeoObject.class).getResultList();	
		return res;
	}

	@SuppressWarnings("unchecked")
	public List<GeoObject> getAllGeoObjects(){
//		String query = "{ " + positionField + " : { $near : " + position.toString() + " , $maxDistance : " + distance + "  } }";
//		query = "{ $and: [" + query + ",{$or:[{objectProvider:'SELF'},{ objectProvider: { $exists: false } }]}]}";
		List<GeoObject> res = entityManager.createNativeQuery(filterSelf, GeoObject.class).getResultList();	
		return res;
	}

	@Override
	public List<GeoObject> getGeoObjectsForUserId(String userId) {
		return super.getNativeList("\"author.id\"", "{$oid: \"" + userId + "\"}");
	}

	public GeoObject getGeoObjectByTitle(String title) {
		List<GeoObject> res = getList("title", title);
		if(res.size() > 0)
			return res.get(0);
		else
			return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getObjectTypes(){
		return entityManager.createQuery("select type from " + GeoObjectType.class.getName()).getResultList();
	}

	@Override
	public List<GeoObject> getGeoObjectsForPosition(Position position){
		String andString = "";
		if(position.getGeoArea() == null)
			return new ArrayList<GeoObject>();
		GeoArea geoArea = position.getGeoArea();
		if(geoArea.getCountryCode() != null){
			andString += "{ position.geoArea.countryCode : '" + geoArea.getCountryCode() + "'},";
		}
		if(geoArea.getPostalCode() != null){
			andString += "{ position.geoArea.postalCode : '" + geoArea.getPostalCode() + "'},";
		}
		if(geoArea.getAdministrativeArea() != null){
			andString += "{ position.geoArea.administrativeArea : '" + geoArea.getAdministrativeArea() + "'},";
		}
		if(geoArea.getSubAdministrativeArea() != null){
			andString += "{ position.geoArea.subAdministrativeArea : '" + geoArea.getSubAdministrativeArea() + "'},";
		}
		if(geoArea.getLocality() != null){
			andString += "{ position.geoArea.locality : '" + geoArea.getLocality() + "'},";
		}
		if(geoArea.getSubLocality() != null){
			andString += "{ position.geoArea.subLocality : '" + geoArea.getSubLocality() + "'},";
		}
		if(geoArea.getThoroughfare() != null){
			andString += "{ position.geoArea.thoroughfare : '" + geoArea.getThoroughfare() + "'},";
		}
		if(geoArea.getSubThoroughfare() != null){
			andString += "{ position.geoArea.subThoroughfare : '" + geoArea.getSubThoroughfare() + "'},";
		}
		if(andString.length() == 0)
			return new ArrayList<GeoObject>();
		andString = andString.substring(0, andString.length()-1);
		return entityManager.createQuery("{$and:[ " + andString + " ]}", GeoObject.class).getResultList();		
	}

	@SuppressWarnings("rawtypes")
	public GeoObject getByLinkedObjectId(String id){
		List res = getNativeList("linkedObjectId", "\"" + id + "\"");
		if(res.size() > 0)
			return (GeoObject) res.get(0);
		else
			return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<GeoObject> getOblectsForLinkedIds(List<String> objects){
		for (int i = 0; i< objects.size();i++) {
			objects.set(i, "{linkedObjectId:" + objects.get(i) + "}");
		}
		String query = StringUtils.join(objects, ',');
		return entityManager.createNativeQuery("{$or:["+query+"]}", GeoObject.class).getResultList();		
	}

}