package risto.data.model.geoObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.FetchType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.springframework.format.annotation.DateTimeFormat;

import risto.data.model.Position;
import risto.data.model.user.UserRef;

@Embeddable
public class SubComment {

	@ElementCollection
	private List<SubComment> subComments;

	@Embedded
	private Position position;// = new Position(0.0, 0.0);
	@DateTimeFormat(pattern="dd.MM.yyyy hh:mm")
    @NotNull @Past
    private Date date = new Date();
//	@NotNull
//    @Size(min = 1, max = 15)
	private String body = "";
	@ElementCollection//(fetch = FetchType.EAGER)
	private List<UserRef> votes;// = new ArrayList<UserRef>();
	@Embedded
//	@NotNull
	private UserRef author;// = new UserRef();
	public Position getPosition() {
		return position;
	}
	public void setPosition(Position position) {
		this.position = position;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public List<UserRef> getVotes() {
		return votes;
	}
	public void setVotes(List<UserRef> votes) {
		this.votes = votes;
	}
	public UserRef getAuthor() {
		return author;
	}
	public void setAuthor(UserRef author) {
		this.author = author;
	}
	public List<SubComment> getSubComments() {
		return subComments;
	}
	public void setSubComments(List<SubComment> subComments) {
		this.subComments = subComments;
	}
}