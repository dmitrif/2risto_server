package risto.data.model.geoObject;

import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;

import risto.data.model.user.UserRef;
import risto.misc.Constants;

import com.fasterxml.jackson.annotation.JsonIgnore;

//@Entity
@Embeddable
public class GeoObjectRef {

	private String title;

	private String provider;

	private String imagePath;

//	@OneToOne(fetch = FetchType.LAZY)
//	@JsonIgnore
//	private GeoObject geoObject;

//	@Type(type = "objectid")
	private String id;

	private String type;

	@Override
	@JsonIgnore
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	@JsonIgnore
	public boolean equals(Object o){
	    if(o == null)
	    	return false;
	    if(!(o instanceof GeoObjectRef))
	    	return false;
	    GeoObjectRef other = (GeoObjectRef) o;
	    return this.id.equals(other.id);
	}

	public GeoObjectRef(GeoObject geoObject) {
		this.title = geoObject.getTitle();
		this.imagePath = geoObject.getRealImagePath();
		if(geoObject.getObjectProvider() != null && !geoObject.getObjectProvider().equals("SELF")){
			this.provider = geoObject.getObjectProvider();
			this.id = geoObject.getLinkedObjectId();
		}else{
			this.id = geoObject.getId();
		}
		this.type = geoObject.getType();
	}

	public GeoObjectRef(GeoObject geoObject, String objectTitle) {
		this.title = objectTitle;
		this.imagePath = geoObject.getRealImagePath();
		this.type = geoObject.getType();
		if(geoObject.getObjectProvider() != null && !geoObject.getObjectProvider().equals("SELF")){
			this.provider = geoObject.getObjectProvider();
			this.id = geoObject.getLinkedObjectId();
		}else{
			this.id = geoObject.getId();
		}
	}

	public GeoObjectRef() {
	}

	public String getId(){
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

//	public GeoObject getGeoObject() {
//		return geoObject;
//	}
//
//	public void setGeoObject(GeoObject geoObject) {
//		this.geoObject = geoObject;
//	}

	public String getImagePath() {
		if(imagePath == null)
			return Constants.getDefaultImagePathForObjectType(type);
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}
}
