package risto.data.model.geoObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import risto.data.model.Position;
import risto.data.model.user.UserRef;

@Entity
@Table(name = "comments")
public class Comment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Type(type = "objectid")
	private String id;

//	@ManyToOne(fetch = FetchType.LAZY)
//	@JsonIgnore
//	private GeoObject geoObject;
	
    @Type(type = "objectid")
    @Column(name = "object_id")
	private String objectId;

    @OneToMany(mappedBy="parent", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Comment> subComments;// = new ArrayList<Comment>();

    @ManyToOne
    @JoinColumn(name="parent_id")
    private Comment parent;

//	@Embedded
//	private Position position;// = new Position(0.0, 0.0);
	@DateTimeFormat(pattern="dd.MM.yyyy hh:mm")
    @NotNull @Past
    private Date date = new Date();
//	@NotNull
//    @Size(min = 1, max = 15)
	private String body;// = "";
	@ElementCollection(fetch = FetchType.EAGER)
	private List<UserRef> votes;// = new ArrayList<UserRef>();
	@Embedded
//	@NotNull
	private UserRef author;// = new UserRef();
//	@Column(name="geoObject_id", updatable=false, insertable=false)
//	@Type(type = "objectid")
//	private String geoObject_fk;
	
	public String getObjectId(){
		return this.objectId;
	}
	
	public void setObjectId(String objectId){
		this.objectId = objectId;
	}
	
//	public Position getPosition() {
//		return position;
//	}
//	public void setPosition(Position position) {
//		this.position = position;
//	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public List<UserRef> getVotes() {
		return votes;
	}
	public void setVotes(List<UserRef> votes) {
		this.votes = votes;
	}
	public UserRef getAuthor() {
		return author;
	}
	public void setAuthor(UserRef author) {
		this.author = author;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
//	public GeoObject getGeoObject() {
//		return geoObject;
//	}
//	public void setGeoObject(GeoObject geoObject) {
//		this.geoObject = geoObject;
//	}
//
	public List<Comment> getSubComments() {
		return subComments;
	}

	public void setSubComments(List<Comment> subComments) {
		this.subComments = subComments;
	}

	public Comment getParent() {
		return parent;
	}

	public void setParent(Comment parent) {
		this.parent = parent;
	}
}
