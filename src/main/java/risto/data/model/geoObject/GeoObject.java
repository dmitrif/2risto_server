package risto.data.model.geoObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import risto.data.model.Position;
import risto.data.model.user.UserRef;
import risto.misc.Constants;

@Entity
@Table(name = "objects")
public class GeoObject{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Type(type = "objectid")
	private String id;
	@Embedded
//	@NotNull
	private UserRef author;// = new UserRef();
//	@NotBlank
    @Size(min = 3)
	private String title;// = "[?]";
	private String type = "EVENT";
	private String description;
	private String imagePath;
	private String objectProvider = "SELF";
	private String linkedObjectId;
	@Embedded
	private Position position;// = new Position(0.0, 0.0);
	@DateTimeFormat(pattern = "dd.MM.yyyy hh:mm")
	private Date date;// = new Date();
	@DateTimeFormat(pattern = "dd.MM.yyyy hh:mm")
	private Date startDate;// = new Date();
	@DateTimeFormat(pattern = "dd.MM.yyyy hh:mm")
	private Date endDate;// = new Date();
	@ElementCollection(fetch = FetchType.EAGER)
	private List<String> photoPathes;// = new ArrayList<String>();
	private Byte rating = 0;
//	@Embedded
	@ElementCollection(fetch = FetchType.EAGER)
	private Set<String> tags;// = new HashSet<String>();
	@ElementCollection(fetch = FetchType.EAGER)
	private List<UserRef> votes;// = new ArrayList<UserRef>();
	private Byte commentsCount = 0;

	public GeoObject(String id){
		this.id = id;
	}

	public GeoObject(){
	}

	public UserRef getAuthor() {
		return author;
	}
	public void setAuthor(UserRef author) {
		this.author = author;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Position getPosition() {
		return position;
	}
	public void setPosition(Position position) {
		this.position = position;
	}
//	public List<Comment> getComments() {
//		return comments;
//	}
//	public void setComments(List<Comment> comments) {
//		this.comments = comments;
//	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public List<String> getPhotoPathes() {
		return photoPathes;
	}
	public void setPhotoPathes(List<String> photoPathes) {
		this.photoPathes = photoPathes;
	}
	public Byte getRating() {
		return rating;
	}
	public void setRating(Byte rating) {
		this.rating = rating;
	}
	public List<UserRef> getVotes() {
		return votes;
	}
	public void setVotes(List<UserRef> votes) {
		this.votes = votes;
	}
	public Set<String> getTags() {
		return tags;
	}
	public void setTags(Set<String> tags) {
		this.tags = tags;
	}
	public String getId() {
		return id;
	}
	public Byte getCommentsCount() {
		return commentsCount;
	}
	public void setCommentsCount(Byte commentsCount) {
		this.commentsCount = commentsCount;
	}

	public String getImagePath() {
		if(imagePath == null)
			return Constants.getDefaultImagePathForObjectType(type);
		return imagePath;
	}

	public String getRealImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getObjectProvider() {
		return objectProvider;
	}
	public void setObjectProvider(String objectProvider) {
		this.objectProvider = objectProvider;
	}
	public String getLinkedObjectId() {
		return linkedObjectId;
	}
	public void setLinkedObjectId(String linkedObjectId) {
		this.linkedObjectId = linkedObjectId;
	}
}