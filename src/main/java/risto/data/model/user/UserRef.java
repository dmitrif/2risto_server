package risto.data.model.user;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import org.hibernate.annotations.Type;

import risto.misc.Constants;

import com.fasterxml.jackson.annotation.JsonIgnore;

//@Entity
@Embeddable
public class UserRef {

//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Type(type = "objectid")
	private String id = String.valueOf(Math.random());

	private String imagePath;

	private String username;

	@Override
	@JsonIgnore
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	@JsonIgnore
	public boolean equals(Object o){
	    if(o == null)
	    	return false;
	    if(!(o instanceof UserRef))
	    	return false;
	    UserRef other = (UserRef) o;
	    return this.id.equals(other.id);
	}
//	@OneToMany(fetch = FetchType.LAZY)
//	@JsonIgnore
//	private User user;

	public UserRef(User user) {
		this.id = user.getId();
//		this.user = user;
		this.username = user.getUsername();
		this.imagePath = user.getRealImagePath();
	}

	public UserRef() {
	}

	public String getUsername() {
		return username;
	}

	public String getId(){
		return id;
//		return user.getId();
	}
	
//	public User getUser() {
//		return user;
//	}
//
//	public void setUser(User user) {
//		this.user = user;
//	}

	public void setId(String id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getImagePath() {
		if(imagePath == null)
			return Constants.DEFAULT_USER_IMAGE_PATH;
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
}
