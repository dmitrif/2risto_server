package risto.data.model.user;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import risto.misc.Constants;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import risto.data.model.Position;
import risto.data.model.geoObject.GeoObjectRef;

@Entity
@Table(name = "users")
//@Access(AccessType.FIELD)
public class User{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Type(type = "objectid")
//	@Access(AccessType.PROPERTY)
	private String id;

	private String firstName;// = "";
	private String lastName;// = "";
	@NotNull(message="Please fill up the sex field")
//	@Enumerated(EnumType.STRING)
	private Constants.Sex sex;// = Constants.Sex.MALE;//is male?
	@NotBlank
    @Size(min = 3, max = 15)
	private String username = "";
	@NotBlank
    @Email
    @JsonIgnore
	private String email = "";
	@JsonIgnore
//	@Enumerated(EnumType.STRING)
	private Constants.AccessRole role = Constants.AccessRole.ROLE_USER;
	@JsonIgnore
	@NotBlank
    @Size(min = 6, message="Password cannot be less then 6 characters")
	private String password;
//	@JsonIgnore
	@Embedded
	private Position initialPosition;// = new Position(0.0, 0.0);
	@JsonIgnore
	@DateTimeFormat(pattern="dd.MM.yyyy hh:mm")
    @NotNull @Past
    private Date registrationDate =  new Date();//.currentTimeMillis() / 1000L;
//	@JsonIgnore
	@Embedded
	private Position position;// = new Position(0.0, 0.0);
//	@JsonIgnore
	private String imagePath;
	@ElementCollection(fetch = FetchType.EAGER)
	private Set<String> votes;
	@JsonIgnore
	private String language = "en";

	@ElementCollection(fetch = FetchType.EAGER)
	private Set<GeoObjectRef> fObjects;// = new HashSet<ObjectRef>();

//	@Embedded
	@ElementCollection(fetch = FetchType.EAGER)
//	@OneToMany//(fetch = FetchType.EAGER)
//	private List<UserRef> fUsers;// = new HashSet<UserRef>();
	private Set<UserRef> fUsers;
//	@OneToMany//(mappedBy = "author.user")
//	private List<GeoObject> geoObjects;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Constants.Sex getSex() {
		return sex;
	}

	public void setSex(Constants.Sex sex) {
		this.sex = sex;
	}

	@JsonIgnore
	public String getEmail() {
		return email;
	}

	@JsonProperty
	public void setEmail(String email) {
		this.email = email;
	}

	public Position getInitialPosition() {
		return initialPosition;
	}

	public void setInitialPosition(Position initialPosition) {
		this.initialPosition = initialPosition;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Constants.AccessRole getRole() {
		return role;
	}

	public void setRole(Constants.AccessRole role) {
		this.role = role;
	}

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getId(){
		return id;
	}

	public void setId(String id){
		this.id = id;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getImagePath() {
		if(imagePath == null)
			return Constants.DEFAULT_USER_IMAGE_PATH;
		return imagePath;
	}

	String getRealImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public Set<GeoObjectRef> getfObjects() {
		return fObjects;
	}

	public void setfObjects(Set<GeoObjectRef> fObjects) {
		this.fObjects = fObjects;
	}

	public Set<UserRef> getfUsers() {
		return fUsers;
	}

	public void setfUsers(Set<UserRef> fUsers) {
		this.fUsers = fUsers;
	}

	public Set<String> getVotes() {
		return votes;
	}

	public void setVotes(Set<String> votes) {
		this.votes = votes;
	}
}