package risto.data.model.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "checkins")
public class Checkin{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Type(type = "objectid")
	private String id;

//	@Override
//	@JsonIgnore
//	public int hashCode() {
//		return id.hashCode();
//	}

//	@Override
//	@JsonIgnore
//	public boolean equals(Object o){
//	    if(o == null)
//	    	return false;
//	    if(!(o instanceof Checkin))
//	    	return false;
//	    Checkin other = (Checkin) o;
//	    return this.userId.equals(other.userId) && this.objectId.equals(other.objectId);
//	}
//	
    @Type(type = "objectid")
    @Column(name = "user_id")
	private String userId;// = "";
    @Type(type = "objectid")
    @Column(name = "object_id")
	private String objectId;// = "";
	
	@DateTimeFormat(pattern="dd.MM.yyyy hh:mm")
    @NotNull @Past
    private Date date =  new Date();

	public Checkin(){
	}
	
	public Checkin(String objectId, String userId){
		this.objectId = objectId;
		this.userId = userId;
//		this.id = objectId + userId;
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
//		this.id = objectId + userId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getObjectId() {
		return objectId;
	}
	public void setObjectId(String objectId) {
		this.objectId = objectId;
//		this.id = objectId + userId;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
}