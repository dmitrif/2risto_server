package risto.data.model.user;

import java.util.Date;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import risto.data.model.Position;

@Entity
@Table(name = "messages")
public class Message{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Type(type = "objectid")
	private String id;
	@Embedded
	private UserRef from;
	@ElementCollection(fetch = FetchType.EAGER)
	private Set<UserRef> to;
	@DateTimeFormat(pattern="dd.MM.yyyy hh:mm")
    @NotNull @Past
    private Date date =  new Date();//.currentTimeMillis() / 1000L;
	@Embedded
	private Position position;// = new Position(0.0, 0.0);
	private String body;// = "";
	private boolean seen = false;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public UserRef getFrom() {
		return from;
	}
	public void setFrom(UserRef from) {
		this.from = from;
	}
	public Set<UserRef> getTo() {
		return to;
	}
	public void setTo(Set<UserRef> to) {
		this.to = to;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Position getPosition() {
		return position;
	}
	public void setPosition(Position position) {
		this.position = position;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public boolean isSeen() {
		return seen;
	}
	public void setSeen(boolean seen) {
		this.seen = seen;
	}
}