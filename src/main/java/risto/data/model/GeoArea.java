package risto.data.model;

import javax.persistence.Embeddable;

@Embeddable
public class GeoArea {
//	Nice
//	ISOcountryCode = FR
//	country = France
//	postalCode = 06000
//	administrativeArea = Provence-Alpes-Côte d'Azur
//	subAdministrativeArea = Alpes-Maritimes
//	locality = Nice
//	subLocality = Palais des Congres
//	thoroughfare = Rue de l'Hôtel des Postes
//	subThoroughfare = 27
//	region = CLCircularRegion (identifier:'<+43.69964080,+7.27272820> radius 70.84', center:<+43.69964080,+7.27272820>, radius:70.84m)
//	inlandWater = (null)
//	ocean = (null)
//	areasOfInterest = (null)
//
//	My house
//	ISOcountryCode = RU
//	country = Russia
//	postalCode = 420000
//	administrativeArea = Republic of Tatarstan
//	subAdministrativeArea = Kazan'
//	locality = Kazan'
//	subLocality = (null)
//	thoroughfare = Serle ulitsa
//	subThoroughfare = 25
//	region = CLCircularRegion (identifier:'<+55.74380180,+49.24061920> radius 70.79', center:<+55.74380180,+49.24061920>, radius:70.79m)
//	inlandWater = (null)
//	ocean = (null)
//	areasOfInterest = (null)
//
//	Мой дом
//	ISOcountryCode = CH
//	country = Switzerland
//	postalCode = 1217
//	administrativeArea = Geneva
//	subAdministrativeArea = Geneva
//	locality = Meyrin
//	subLocality = Meyrin
//	thoroughfare = Avenue Sainte-Cécile
//	subThoroughfare = 33–43
//	region = CLCircularRegion (identifier:'<+46.23637889,+6.08283070> radius 141.66', center:<+46.23637889,+6.08283070>, radius:141.66m)
//	inlandWater = (null)
//	ocean = (null)
//	areasOfInterest = (null)

	private String countryCode;
	private String postalCode;
	private String administrativeArea;
	private String subAdministrativeArea;
	private String locality;
	private String subLocality;
	private String thoroughfare;
	private String subThoroughfare;

    public GeoArea() {
    }
    
	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getAdministrativeArea() {
		return administrativeArea;
	}

	public void setAdministrativeArea(String administrativeArea) {
		this.administrativeArea = administrativeArea;
	}

	public String getSubAdministrativeArea() {
		return subAdministrativeArea;
	}

	public void setSubAdministrativeArea(String subAdministrativeArea) {
		this.subAdministrativeArea = subAdministrativeArea;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getSubLocality() {
		return subLocality;
	}

	public void setSubLocality(String subLocality) {
		this.subLocality = subLocality;
	}

	public String getThoroughfare() {
		return thoroughfare;
	}

	public void setThoroughfare(String thoroughfare) {
		this.thoroughfare = thoroughfare;
	}

	public String getSubThoroughfare() {
		return subThoroughfare;
	}

	public void setSubThoroughfare(String subThoroughfare) {
		this.subThoroughfare = subThoroughfare;
	}
}