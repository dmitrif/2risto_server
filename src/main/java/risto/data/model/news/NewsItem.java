package risto.data.model.news;

import java.util.Date;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.springframework.format.annotation.DateTimeFormat;

import risto.data.model.Position;
import risto.data.model.geoObject.GeoObjectRef;
import risto.data.model.user.UserRef;
import risto.misc.Constants.News;

@Entity
@Table(name = "news")
public class NewsItem {

	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Type(type = "objectid")
	private String id;
	
	@NotNull
	private News type = News.NEW_OBJECT;

	@Embedded
	private GeoObjectRef object;

	@Embedded
	private Position position;// = new Position(0.0, 0.0);

	@DateTimeFormat(pattern="dd.MM.yyyy hh:mm")
    @NotNull @Past
    private Date date = new Date();
	
	private String body = "";
	
	@Embedded
	private UserRef author = new UserRef();

	public NewsItem(GeoObjectRef geoObjectRef, UserRef userRef, News type){
		this.object = geoObjectRef;
		this.type = type;
		this.author = userRef;
		this.id = geoObjectRef.getId() + author.getId() + type.ordinal();
	}
	
	public NewsItem(){
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public GeoObjectRef getObject() {
		return object;
	}

	public void setObject(GeoObjectRef object) {
		this.object = object;
		this.id = object.getId() + author.getId() + type.ordinal();
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public UserRef getAuthor() {
		return author;
	}

	public void setAuthor(UserRef author) {
		this.author = author;
	}

	public News getType() {
		return type;
	}

	public void setType(News type) {
		this.type = type;
		this.id = object.getId() + author.getId() + type.ordinal();
	}
}
