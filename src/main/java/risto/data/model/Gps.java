package risto.data.model;

import javax.persistence.Embeddable;

@Embeddable
public class Gps {

	private Double lon;
	private Double lat;
	
    public Gps(double lon, double lat) {
    	this.lon = lon;
    	this.lat = lat;
    }
    
    public Gps() {
//    	this.lon = 0.0;
//    	this.lat = 0.0;
    }
    
	public Double getLon() {
		return lon;
	}
	
	public void setLon(Double lon) {
		this.lon = lon;
	}
	
	public Double getLat() {
		return lat;
	}
	
	public void setLat(Double lat) {
		this.lat = lat;
	}

	public String toString(){
		return "[" + lon + "," + lat + "]";
	}

}