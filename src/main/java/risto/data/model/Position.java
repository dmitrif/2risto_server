package risto.data.model;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.ManyToOne;

@Embeddable
public class Position {

	public GeoArea getGeoArea() {
		return geoArea;
	}

	public void setGeoArea(GeoArea geoArea) {
		this.geoArea = geoArea;
	}

	@Embedded
	private Gps gps;
	
//	@ManyToOne
	private GeoArea geoArea;

//	private String countryCode;
//	private String postalCode;
//	private String administrativeArea;
//	private String subAdministrativeArea;
//	private String locality;
//	private String subLocality;
//	private String thoroughfare;
//	private String subThoroughfare;

    public Position(double lon, double lat) {
    	this.setGps(new Gps(lon, lat));
    }
    
    public Position() {
    }
    
//	public String getCountryCode() {
//		return countryCode;
//	}
//
//	public void setCountryCode(String countryCode) {
//		this.countryCode = countryCode;
//	}
//
//	public String getPostalCode() {
//		return postalCode;
//	}
//
//	public void setPostalCode(String postalCode) {
//		this.postalCode = postalCode;
//	}
//
//	public String getAdministrativeArea() {
//		return administrativeArea;
//	}
//
//	public void setAdministrativeArea(String administrativeArea) {
//		this.administrativeArea = administrativeArea;
//	}
//
//	public String getSubAdministrativeArea() {
//		return subAdministrativeArea;
//	}
//
//	public void setSubAdministrativeArea(String subAdministrativeArea) {
//		this.subAdministrativeArea = subAdministrativeArea;
//	}
//
//	public String getLocality() {
//		return locality;
//	}
//
//	public void setLocality(String locality) {
//		this.locality = locality;
//	}
//
//	public String getSubLocality() {
//		return subLocality;
//	}
//
//	public void setSubLocality(String subLocality) {
//		this.subLocality = subLocality;
//	}
//
//	public String getThoroughfare() {
//		return thoroughfare;
//	}
//
//	public void setThoroughfare(String thoroughfare) {
//		this.thoroughfare = thoroughfare;
//	}
//
//	public String getSubThoroughfare() {
//		return subThoroughfare;
//	}
//
//	public void setSubThoroughfare(String subThoroughfare) {
//		this.subThoroughfare = subThoroughfare;
//	}

	public String toString(){
		return "[" + gps.getLon() + "," + gps.getLat() + "]";
	}

	public Gps getGps() {
		return gps;
	}

	public void setGps(Gps gps) {
		this.gps = gps;
	}

}