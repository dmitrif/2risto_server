package risto.data.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import risto.data.dao.CommentDao;
import risto.data.dao.GeoObjectDao;
import risto.data.dao.NewsDao;
import risto.data.dao.UserDao;
import risto.data.model.Position;
import risto.data.model.geoObject.Comment;
import risto.data.model.geoObject.GeoObject;
import risto.data.model.geoObject.GeoObjectRef;
import risto.data.model.news.NewsItem;
import risto.data.model.user.User;
import risto.misc.Constants.News;
import risto.misc.UtilityService;

@Service
public class GeoObjectService{

	@Autowired
	@Qualifier("geoObjectDao") 
	private GeoObjectDao geoObjectDao;

	@Autowired
	@Qualifier("commentDao") 
	private CommentDao commentDao;

	@Autowired
	@Qualifier("userDao") 
	private UserDao userDao;

	@Autowired
	@Qualifier("newsDao") 
	private NewsDao newsDao;

	@Autowired
	private UtilityService utilityService;

	public GeoObject getGeoObjectById(String id) {
		return geoObjectDao.getById(id);
	}

	public GeoObject getGeoObjectByLinkedObjectId(String id) {
		return geoObjectDao.getByLinkedObjectId(id);
	}

	public GeoObject getGeoObjectByTitle(String title) {
		return geoObjectDao.getGeoObjectByTitle(title);
	}

	@Transactional
	public void update(GeoObject geoObject) {
		geoObjectDao.add(geoObject);
	}

	@Transactional
	public void add(GeoObject geoObject) {
		geoObjectDao.add(geoObject);
		if(geoObject.getObjectProvider().equals("SELF")){
			NewsItem newsItem = new NewsItem(new GeoObjectRef(geoObject), geoObject.getAuthor(),News.NEW_OBJECT);
			newsItem.setPosition(geoObject.getPosition());
			newsItem.setBody("New " + geoObject.getType().toLowerCase() + " added");
			newsDao.add(newsItem);
		}
	}

	public List<GeoObject> getGeoObjectsInRange(Position position, Double distance){
		return geoObjectDao.getGeoObjectsInRange("position.gps", position, distance);
	}

	public List<GeoObject> getOblectsForLinkedIds(List<String> objects){
		return geoObjectDao.getOblectsForLinkedIds(objects);
	}

	public List<GeoObject> getGeoObjectsForPosition(Position position) {
		return geoObjectDao.getGeoObjectsForPosition(position);
	}

	public List<GeoObject> getAllGeoObjects(){
		return geoObjectDao.getAllGeoObjects();
	}

	public void addComment(Comment comment, String parentId) {
		addComment(comment, parentId, null);
	}
	
	@Transactional
	public void addComment(Comment comment, String parentId, String title) {
		if(parentId != null){
			Comment parent = commentDao.getById(parentId);
			if(parent.getSubComments() == null)
				parent.setSubComments(new ArrayList<Comment>());
			parent.getSubComments().add(comment);
			comment.setParent(parent);
		}
		commentDao.add(comment);
		GeoObject geoObject = geoObjectDao.getById(comment.getObjectId());
		geoObject.setCommentsCount((byte) (geoObject.getCommentsCount() + 1));
		geoObject.setRating((byte) (geoObject.getRating() + 1));
		if(geoObject.getLinkedObjectId() != null && geoObject.getObjectProvider().equals("SELF")){
			GeoObject rObject = geoObjectDao.getById(geoObject.getLinkedObjectId());
			rObject.setRating((byte) (rObject.getRating() + 1));
			geoObjectDao.update(rObject);
		}
		geoObjectDao.update(geoObject);
		GeoObjectRef geoObjectRef;// = new GeoObjectRef(geoObject);
		if(title != null){
			geoObjectRef = new GeoObjectRef(geoObject, title);
		}else{
			geoObjectRef = new GeoObjectRef(geoObject);
		}
		NewsItem newsItem = new NewsItem(geoObjectRef, comment.getAuthor(), News.NEW_COMMENT);
		newsItem.setPosition(geoObject.getPosition());
		newsItem.setBody("New comments added: " + comment.getBody());
		newsItem.setDate(new Date());
		newsDao.update(newsItem);
		if(geoObject.getAuthor() != null){
			User user = userDao.getById(geoObject.getAuthor().getId());
			utilityService.sendEmail(user.getEmail(), "New Comment from [ " + comment.getAuthor().getUsername() + " ]", "Hello " + user.getFirstName() + " " + user.getLastName()
					+ "\n\nYou have new comment for [ " + geoObject.getTitle() + " ]:\n\n" +
					comment.getBody());
		}
	}
	
	public List<Comment> getCommentsForGeoObjectId(String id){
		return commentDao.getCommentsForGeoObjectId(id);
	}
	
	public List<String> getObjectTypes(){
		return geoObjectDao.getObjectTypes();
	}

	@Transactional
	public void deleteObject(GeoObject geoObject) {
		geoObjectDao.delete(geoObject);
	}
}