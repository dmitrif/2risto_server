package risto.data.service;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import risto.data.dao.CheckinDao;
import risto.data.dao.GeoObjectDao;
import risto.data.dao.MessageDao;
import risto.data.dao.NewsDao;
import risto.data.dao.UserDao;
import risto.data.model.Position;
import risto.data.model.geoObject.GeoObject;
import risto.data.model.geoObject.GeoObjectRef;
import risto.data.model.news.NewsItem;
import risto.data.model.user.Checkin;
import risto.data.model.user.Message;
import risto.data.model.user.User;
import risto.data.model.user.UserRef;
import risto.misc.Constants.News;

@Service
public class UserService{

	@Autowired
	@Qualifier("userDao") 
	private UserDao userDao;

	@Autowired
	@Qualifier("checkinDao") 
	private CheckinDao checkinDao;

	@Autowired
	@Qualifier("messageDao") 
	private MessageDao messageDao;

	@Autowired
	@Qualifier("geoObjectDao") 
	private GeoObjectDao geoObjectDao;

	@Autowired
	@Qualifier("newsDao") 
	private NewsDao newsDao;

	public User getUserByEmail(String email) {
		return userDao.getUserByEmail(email);
	}

	public User getUserById(String id) {
		return userDao.getById(id);
	}

	public User getUserByUsername(String username) {
		return userDao.getUserByUsername(username);
	}

	@Transactional
	public void saveOrUpdate(User user) {
		
		userDao.add(user);
	}

	@Transactional
	public void deleteUser(User user) {
		userDao.delete(user);
	}
	
	public List<User> getUsersInRange(Position position, Double distance){
		return userDao.getUsersInRange(position, distance);
	}

	public List<NewsItem> getNewsInRange(Position position, Double distance){
		return newsDao.getNewsInRange(position, distance);
	}

	public List<GeoObject> getGeoObjectsForUserId(String id){
		return geoObjectDao.getGeoObjectsForUserId(id);
	}

	@Transactional
	public void followObject(User user, GeoObject geoObject) {
		Set<GeoObjectRef> fObjects = user.getfObjects();
		fObjects.add(new GeoObjectRef(geoObject));
		userDao.update(user);
	}

	@Transactional
	public void followUser(User user, User fUser) {
		Set<UserRef> fUsers = user.getfUsers();
		fUsers.add(new UserRef(fUser));
		userDao.update(user);
	}

	@Transactional
	public void unfollowObject(User user, GeoObject geoObject) {
		Set<GeoObjectRef> fObjects = user.getfObjects();
		fObjects.remove(new GeoObjectRef(geoObject));
		userDao.update(user);
	}

	@Transactional
	public void unfollowUser(User user, User fUser) {
		Set<UserRef> fUsers = user.getfUsers();
		fUsers.remove(new UserRef(fUser));
		userDao.update(user);
	}

	@Transactional
	public void add(Message message) {
		List<User> users = userDao.getUsersFromUserRefSet(message.getTo());
		Set<UserRef> userRefs = new HashSet<UserRef>();
		for(User user : users){
			userRefs.add(new UserRef(user));
		}
		message.setTo(userRefs);
		messageDao.add(message);
	}

	@Transactional
	public void update(Message message) {
		messageDao.update(message);
	}

	public List<Message> getMessagesFromUser(User user){
		return messageDao.getMessagesFromUser(user.getId());
	}
	
	public List<Message> getMessagesToUser(User user){
		return messageDao.getMessagesToUser(user.getId());
	}

	public List<Message> getMessagesWithUser(User user){
		return messageDao.getMessagesWithUser(user.getId());
	}

	public Message getMessage(String id) {
		return messageDao.getById(id);
	}

	public List<NewsItem> getNewsForUser(User user) {
		return newsDao.getNewsForUser(user);
	}

	public List<NewsItem> getNewsForPosition(Position position) {
		return newsDao.getNewsForPosition(position);
	}

	public String checkinUserAtObject(User user, String objectId) {
		return checkinUserAtObject(user, objectId, null);
	}

	@Transactional
	public String checkinUserAtObject(User user, String objectId, String objectTitle) {
		if(checkinDao.isCheckinExists(user.getId(), objectId))
			return null;
		GeoObjectRef geoObjectRef;// = new GeoObjectRef(geoObject);
		GeoObject geoObject = geoObjectDao.getById(objectId);
		if(objectTitle != null){
			geoObjectRef = new GeoObjectRef(geoObject, objectTitle);
		}else{
			geoObjectRef = new GeoObjectRef(geoObject);
		}
		Checkin checkin = new Checkin();
		checkin.setDate(new Date());
		checkin.setObjectId(objectId);
		checkin.setUserId(user.getId());
		checkinDao.add(checkin);
		NewsItem newsItem = new NewsItem(geoObjectRef, new UserRef(user), News.NEW_CHEKIN);
		newsItem.setBody("User " + user.getUsername() + " checked in at " + objectTitle);
		newsItem.setPosition(geoObject.getPosition());
		newsDao.add(newsItem);
		return newsItem.getId();
	}

	public List<Checkin> getCheckinsForUser(String id){
		return checkinDao.getCheckinsForUser(id);
	}

	public List<NewsItem> getAllNews() {
		return newsDao.getAllNews();
	}

	
//	@Transactional
//	public void setMessageSeen(Message message) {
//		message.setSeen(true);
//		messageDao.saveOrUpdate(message);
//	}
}