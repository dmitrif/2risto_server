package utils;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
 
@Component
@EnableWebMvc
@Configuration
@ComponentScan({ "risto.*" })
//@Import({ WebSecurityConfig.class })
public class ServerConfig {
//	@Bean
//	public SessionFactory dataSessionFactory(){
//		return DatabaseConfig.getOgmSessionFactory();
//	}
}