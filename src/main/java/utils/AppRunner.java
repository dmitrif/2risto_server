package utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import risto.config.AppConfig;
import risto.config.init.SpringMvcInitializer;
import risto.data.model.Position;
import risto.data.model.user.User;
import risto.data.service.GeoObjectService;
import risto.data.service.UserService;

@Configuration
//@ComponentScan({ "risto.*" })
@ComponentScan(basePackages = {"risto.*"}, 
excludeFilters = {@ComponentScan.Filter(value = AppConfig.class, type = FilterType.ASSIGNABLE_TYPE),@ComponentScan.Filter(value = SpringMvcInitializer.class, type = FilterType.ASSIGNABLE_TYPE)})

public class AppRunner {
	
	@Autowired
	UserService userService;

	@Autowired
	GeoObjectService geoObjectService;
	
	public void run(){
		User user = new User();
		user.setUsername("user1");
		user.setEmail("test@ww.ee");
		BCryptPasswordEncoder bc = new BCryptPasswordEncoder();
		user.setPassword(bc.encode("testpassword"));
//		user.setRole("ROLE_USER");
		user.setInitialPosition(new Position(3.0, 7.0));

		
		User user2 = new User();
		user2.setUsername("user2");
		user2.setEmail("test2@ww.ee");
//		bc = new BCryptPasswordEncoder();
		user2.setPassword(bc.encode("testpassword2"));
//		user2.setRole("ROLE_USER");
		user2.setInitialPosition(new Position(4.0, 4.0));
		
		userService.saveOrUpdate(user);
		userService.saveOrUpdate(user2);
//		List<User> rr = userService.getUsersInRange(new Position(4.0, 4.0), 5000.0);
//
//		GeoObject geoObject = new GeoObject();
//		Author author = new Author();
//		author.setUser(user);
//		author.setLogin("user1");
//		Author author2 = new Author();
//		author2.setUser(user);
//		author2.setLogin("user12");
//		geoObject.setAuthor(author);
//		HashSet<String> tags = new HashSet<String>();
//		tags.add("usertag1");
//		tags.add("usertag2");
//		geoObject.setTags(tags);
//		
//		
////		Comment comment = new Comment();
////		comment.setAuthor(author);
//		userService.addUser(user);
//		List<Author> votes = new ArrayList<Author>();
//		votes.add(author);
//		geoObject.setVotes(votes);
//		geoObjectService.addGeoObject(geoObject);
//		ArrayList<Comment> comments = new ArrayList<Comment>();
//		Comment comment = new Comment();
//		comment.setBody("Comment1");
//		comment.setAuthor(author);
////		comment.setUser(user);
//		comments.add(comment);
//		comment.setBody("comment1");
////		Comment comment2 = new Comment();
////		comment2.setBody("Comment2");
////		comment2.setAuthor(author);
////		comments.add(comment);
////		geoObject.setComments(comments);
//		comment.setVotes(votes);
//
//		comment.setGeoObject(geoObject);
//		List<Author> votes2 = new ArrayList<Author>();
//		votes2.add(author2);
//		comment.setVotes(votes);
//		commentService.addComment(comment);
//
////		user = new User();
////		user.setEmail("user");
////		user.setPassword(bc.encode("password"));
////		user.setRole("ROLE_USER");
////		userService.addUser(user);
	}

	public static void main(String[] args){
		ConfigurableApplicationContext  context = new AnnotationConfigApplicationContext(AppRunner.class);
	    AppRunner appRunner = context.getBean(AppRunner.class);
	    appRunner.run();
	    context.close();
	}
}
