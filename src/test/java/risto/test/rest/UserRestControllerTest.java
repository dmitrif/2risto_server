package risto.test.rest;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.List;

import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import risto.config.AppConfig;
import risto.data.model.Position;
import risto.data.model.geoObject.GeoObject;
import risto.data.model.user.Message;
import risto.data.model.user.User;
import risto.data.model.user.UserRef;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
@Transactional
public class UserRestControllerTest extends AbstractRestTest{

	@Test
	public void getUserInfo(){
		Object ret = performGetRequest("/service/users/username/test");
		assertNotNull(ret);
		String responseString = jsonToString(ret);
		User user = stringToJson(responseString, User.class);
		ret = performGetRequest("/service/users/id/" + user.getId());
		assertNotNull(ret);
		prettyWrite(ret);
	}

	@Test
	public void checkinUser(){
//		Position position = new Position(lon, lat);
//		position.setCountryCode("CH");
//		position.setPostalCode("1217");
//		position.setAdministrativeArea("Geneva");
//		position.setSubAdministrativeArea("Geneva");
//		position.setLocality("Meyrin");
//		position.setSubLocality("Meyrin");
//		position.setThoroughfare("Avenue Sainte-Cécile");
//		position.setSubThoroughfare("33–43");
		Object ret = performPutRequest("/service/users/checkin?objectId=" + objectId, "");
		assertNotNull(ret);
		prettyWrite(ret);
		Object ret2 = performPutRequest("/service/users/checkin?objectId=" + objectId, "", 208);
		assertNotNull(ret2);
		prettyWrite(ret2);
	}

	@Test
	public void listUsersInRange(){
		Object ret = performGetRequest("/service/users/nearby?lon=100.0&lat=100.0&dst=100.0");
		assertNotNull(ret);
		prettyWrite(ret);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getObjectsForUser(){
		Object ret = performGetRequest("/service/users/id/" + userId + "/objects");
		assertNotNull(ret);
		String responseString = jsonToString(ret);
		List<GeoObject> objects = stringToJson(responseString, List.class);
		assertTrue(objects.size() > 0);
		prettyWrite(objects);
	}

	@Test
	public void followAndUnfollowObject(){
		performPutRequest("/service/objects/id/" + objectId + "/follow", "");
		Object ret = performGetRequest("/service/users/id/" + userId);
		assertNotNull(ret);
		String responseString = jsonToString(ret);
		User user = stringToJson(responseString, User.class);
		assertTrue(user.getfObjects().size() > 0);
		int count = user.getfObjects().size();
		prettyWrite("fObjects before " + user.getfObjects());
		performPutRequest("/service/objects/id/" + objectId + "/unfollow", "");
		ret = performGetRequest("/service/users/id/" + userId);
		assertNotNull(ret);
		responseString = jsonToString(ret);
		user = stringToJson(responseString, User.class);
		assertTrue(user.getfObjects().size() < count);
		prettyWrite("fObjects after " + user.getfObjects());
	}

	@Test
	public void followAndUnfollowUser(){
		performPutRequest("/service/users/id/" + userId + "/follow", "");
		Object ret = performGetRequest("/service/users/id/" + userId);
		assertNotNull(ret);
		String responseString = jsonToString(ret);
		User user = stringToJson(responseString, User.class);
		assertTrue(user.getfUsers().size() > 0);
		int count = user.getfUsers().size();
		prettyWrite("fUsers before " + user.getfUsers());
		performPutRequest("/service/users/id/" + userId + "/unfollow", "");
		ret = performGetRequest("/service/users/id/" + userId);
		assertNotNull(ret);
		responseString = jsonToString(ret);
		user = stringToJson(responseString, User.class);
		assertTrue(user.getfUsers().size() < count);
		prettyWrite("fUsers after " + user.getfUsers());
	}

	@Test
	public void getNewsfeedForUserSubscriptions(){
		performPutRequest("/service/objects/id/" + objectId + "/follow", "");
		Object ret = performGetRequest("/service/users/id/" + userId);
		assertNotNull(ret);
		String responseString = jsonToString(ret);
		User user = stringToJson(responseString, User.class);
		assertTrue(user.getfObjects().size() > 0);
		prettyWrite("fObjects before " + user.getfObjects());
		ret = performGetRequest("/service/news/followed");
		assertNotNull(ret);
		prettyWrite(ret, "news followed");
	}

	@Test
	public void getNewsfeedForUserExactPosition(){
		Object ret = performGetRequest("/service/news/nearby?lon=100.0&lat=100.0&dst=100.0");
		assertNotNull(ret);
		prettyWrite(ret, "news nearby");
	}

	@Test
	public void getNewsfeedForUserPositionLocalField(){
		String requestParamString = "";
		try {
			requestParamString = URLEncoder.encode("thoroughfare=Avenue Sainte-Cécile&subLocality=Meyrin"+
"&locality=Meyrin&postalCode=1217&countryCode=CH", "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		Position position = new Position();
//		position.setThoroughfare("Avenue Sainte-Cécile");
//		position.setSubLocality("Meyrin");
//		position.setLocality("Meyrin");
//		position.setPostalCode("1217");
//		position.setCountryCode("CH");
		Object ret = performGetRequest("/service/news/area_news?" + requestParamString);
		assertNotNull(ret);
		prettyWrite(ret, "news in Meyrin");
	}

	@Test
	public void getMessages(){
		Object ret = performGetRequest("/service/messages");
		assertNotNull(ret);
		String responseString = jsonToString(ret);
		prettyWrite(responseString);
	}

	@Test
	public void sendMessageToAndReadMessage(){
		Message message = new Message();
		UserRef to = new UserRef();
		to.setId(userId);
		HashSet<UserRef> tos = new HashSet<UserRef>();
		tos.add(to);
		message.setTo(tos);
		message.setBody("test message");
		prettyWrite(message);
		prettyWrite(jsonToString(message));
		String retString = (String) performPostRequest("/service/messages", jsonToString(message));
		entityManager.flush();
		entityManager.clear();
		prettyWrite(retString);
		performPutRequest("/service/messages/" + retString + "/seen", null);
		entityManager.flush();
		entityManager.clear();
		Object ret = performGetRequest("/service/messages");
		assertNotNull(ret);
		prettyWrite(ret, "incoming");
		ret = performGetRequest("/service/messages/sent");
		assertNotNull(ret);
		prettyWrite(ret, "outgoing");
	}
}