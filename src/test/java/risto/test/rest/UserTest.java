package risto.test.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import risto.config.AppConfig;
import risto.web.controller.rest.MainRestController;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public class UserTest {

    private MockMvc mockMvc;
    
    @Autowired 
    MainRestController mainRestController;

    @Before
	public void init(){
    	this.mockMvc =  MockMvcBuilders.standaloneSetup(mainRestController).build();
	}
	
	@Test
	public void login(){
		 try {
			MvcResult result = mockMvc.perform(get("/restlogin")
					.header("X-RST-Username", "test")
					.header("X-RST-Password", "testtest"))
			.andExpect(status().isOk()).andReturn();
			System.out.println("[TEST]" + result.getResponse().getContentAsString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
