package risto.test.rest;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.*;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import risto.config.AppConfig;
import risto.data.model.GeoArea;
import risto.data.model.Position;
import risto.data.model.geoObject.Comment;
import risto.data.model.geoObject.GeoObject;
import risto.data.model.user.User;
import risto.data.model.user.UserRef;
import risto.web.controller.rest.GeoObjectRestController;
import risto.web.controller.rest.MainRestController;
import risto.web.controller.rest.UserRestController;
import risto.web.misc.JsonResponse;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public abstract class AbstractRestTest {

	protected MockMvc mockMvc;
	protected String token;
	protected String userId;
	protected String objectId;
	protected String commentId;
	protected final String title = "test title";
	protected final String username = "test";
	protected final String password = "testtest";
	protected final double lon = 100.0;
	protected final double lat = 100.0;

	private static ObjectWriter prettyWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();

	@PersistenceContext
	protected EntityManager entityManager;

	@Autowired 
	MainRestController mainRestController;

	@Autowired 
	GeoObjectRestController geoObjectRestController;

	@Autowired 
	UserRestController userRestController;

	protected String jsonToString(Object ret) {
		ObjectMapper om = new ObjectMapper();
		try {
			return om.writeValueAsString(ret);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	protected <T> T stringToJson(String string, Class<T> clazz) {
		ObjectMapper om = new ObjectMapper();
		try {
			return om.readValue(string, clazz);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	protected void prettyWrite(Object object, String message){
		try {
			System.out.println("[TEST " + message + " " + Thread.currentThread().getStackTrace()[2].getMethodName() + " ]\n" + prettyWriter.writeValueAsString(object));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

	protected void prettyWrite(Object object){
		try {
			System.out.println("[TEST " + Thread.currentThread().getStackTrace()[2].getMethodName() + " ]\n" + prettyWriter.writeValueAsString(object));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

	protected Object performDeleteRequest(String path){
		return performDeleteRequest(path, 200);
	}

	protected Object performDeleteRequest(String path, int expectedStatus){
		MvcResult result = null;
		try {
			Authentication auth = new UsernamePasswordAuthenticationToken(username,password);
			SecurityContextHolder.getContext().setAuthentication(auth);
			result = mockMvc.perform(delete(path)
					.header("X-RST-Token", token)
					.contentType(MediaType.APPLICATION_JSON))
					.andReturn();
			return processRequest(result, expectedStatus);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	protected Object performPostRequest(String path, String body){
		return performPostRequest(path, body, 201);
	}

	protected Object performPostRequest(String path, String body, int expectedStatus){
		MvcResult result = null;
		try {
			Authentication auth = new UsernamePasswordAuthenticationToken(username,password);
			SecurityContextHolder.getContext().setAuthentication(auth);
			result = mockMvc.perform(post(path)
					.header("X-RST-Token", token)
					.contentType(MediaType.APPLICATION_JSON)
					.content(body))
					.andReturn();
			return processRequest(result, expectedStatus);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	protected Object performPutRequest(String path, String body){
		return performPutRequest(path, body, 200);
	}

	protected Object performPutRequest(String path, String body, int expectedStatus){
		MvcResult result = null;
		try {
			Authentication auth = new UsernamePasswordAuthenticationToken(username,password);
			SecurityContextHolder.getContext().setAuthentication(auth);
			result = mockMvc.perform(put(path)
					.header("X-RST-Token", token)
					.contentType(MediaType.APPLICATION_JSON)
					.content(body))
					.andReturn();
			return processRequest(result, expectedStatus);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	protected Object performGetRequest(String path){
		return performGetRequest(path, 200);
	}

	protected Object performGetRequest(String path, int expectedStatus){
		MvcResult result = null;
		try {
			result = mockMvc.perform(get(path)
					.header("X-RST-Token", token))
					.andReturn();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return processRequest(result, expectedStatus);
	}

	private Object processRequest(MvcResult result, int expectedStatus) {
		try{
			assertEquals(result.getResponse().getStatus(), expectedStatus);
			JsonResponse jsonResponse = new ObjectMapper().readValue(result.getResponse().getContentAsString(), JsonResponse.class);
			return jsonResponse.getBody();
		} catch (AssertionError e) {
			if(result.getResolvedException() == null){
				try {
					JsonResponse re = new ObjectMapper().readValue(result.getResponse().getContentAsString(), JsonResponse.class);
					System.out.println("[TEST ERROR] " + (String)re.getBody());
					fail();
					return re.getBody();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}else
				System.out.println("[TEST ERROR] " + result.getResolvedException().getMessage());
			fail();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Before
	public void init(){
		try {
			mockMvc =  MockMvcBuilders.standaloneSetup(mainRestController, geoObjectRestController, userRestController).build();
			MvcResult result = mockMvc.perform(get("/restlogin")
					.header("X-RST-Username", username)
					.header("X-RST-Password", password))
					.andExpect(status().isOk()).andReturn();
			JsonResponse jsonResponse = new ObjectMapper().readValue(result.getResponse().getContentAsString(), JsonResponse.class);
			token = (String) jsonResponse.getBody();
			Object ret = performGetRequest("/service/users/username/test");
			assertNotNull(ret);
			String responseString = jsonToString(ret);
			User user = stringToJson(responseString, User.class);
			userId = user.getId();

			String geoObjectString = "{\"title\" : \""+title+"\",\"position\" : {\"gps\" : {\"lon\" : "+lon+","+
"\"lat\" : "+lat+"},\"geoArea\" : {"+
"\"countryCode\": \"CH\","+
"\"postalCode\": \"1217\","+
"\"administrativeArea\": \"Geneva\","+
"\"subAdministrativeArea\": \"Geneva\","+
"\"locality\": \"Meyrin\","+
"\"subLocality\": \"Meyrin\","+
"\"thoroughfare\": \"Avenue Sainte-Cécile\","+
"\"subThoroughfare\": \"33–43\""+
"}"+
"}}";
//			GeoObject object = new GeoObject();
			UserRef userRef = new UserRef();
			userRef.setId(userId);
			userRef.setUsername(username);
//			object.setAuthor(userRef);
//			object.setTitle(title);
//			Мой дом
//			ISOcountryCode = CH
//			country = Switzerland
//			postalCode = 1217
//			administrativeArea = Geneva
//			subAdministrativeArea = Geneva
//			locality = Meyrin
//			subLocality = Meyrin
//			thoroughfare = Avenue Sainte-Cécile
//			subThoroughfare = 33–43
//			region = CLCircularRegion (identifier:'<+46.23637889,+6.08283070> radius 141.66', center:<+46.23637889,+6.08283070>, radius:141.66m)
//			inlandWater = (null)
//			ocean = (null)
//			areasOfInterest = (null)

			Position position = new Position(lon, lat);
			GeoArea geoArea = new GeoArea();
			position.setGeoArea(geoArea);
			geoArea.setCountryCode("CH");
			geoArea.setPostalCode("1217");
			geoArea.setAdministrativeArea("Geneva");
			geoArea.setSubAdministrativeArea("Geneva");
			geoArea.setLocality("Meyrin");
			geoArea.setSubLocality("Meyrin");
			geoArea.setThoroughfare("Avenue Sainte-Cécile");
			geoArea.setSubThoroughfare("33–43");
//			object.setPosition(position);
//			ret = performPostRequest("/service/objects", jsonToString(object));
			ret = performPostRequest("/service/objects", geoObjectString);
			objectId = (String)ret;
			//			System.out.println("[TEST] " + token);
			Comment comment = new Comment();
//			UserRef userRef = new UserRef();
//			userRef.setId(userId);
//			userRef.setUsername(username);
			comment.setAuthor(userRef);
			comment.setBody("test comment");
//
//			Object retr = performPostRequest("/service/objects/" + objectId + "/comments", jsonToString(comment));
//			commentId = (String)retr;
//			assertNotNull(ret);
			prettyWrite(ret);
//			entityManager.flush();
//			entityManager.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@After
	public void shutdown(){
		try {
			MvcResult result = mockMvc.perform(get("/restgloballogout")
					.header("X-RST-Username", username)
					.header("X-RST-Password", password))
					.andExpect(status().isOk()).andReturn();
			JsonResponse jsonResponse = new ObjectMapper().readValue(result.getResponse().getContentAsString(), JsonResponse.class);
			assertNotNull(jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
