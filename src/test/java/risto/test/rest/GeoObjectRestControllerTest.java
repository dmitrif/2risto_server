package risto.test.rest;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import risto.config.AppConfig;
import risto.data.model.Position;
import risto.data.model.geoObject.Comment;
import risto.data.model.geoObject.GeoObject;
import risto.data.model.user.UserRef;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
@Transactional
public class GeoObjectRestControllerTest extends AbstractRestTest{
	
	@Test
	public void addAndRemoveObject(){
		GeoObject object = new GeoObject();
//		UserRef userRef = new UserRef();
//		userRef.setId(userId);
//		userRef.setUsername(username);
//		object.setAuthor(userRef);
		object.setTitle(title);
//		object.setType("PLACE");
		object.setPosition(new Position(lon, lat));
		String stringObject = "{  \"title\" : \" string test title\", \"position\" : {\"gps\" : {\"lon\" : 100.0,\"lat\" : 100.0}}}";
		Object ret = performPostRequest("/service/objects", stringObject);
//		Object ret = performPostRequest("/service/objects", jsonToString(object));
		assertNotNull(ret);
		prettyWrite(ret);
		prettyWrite(object);
		ret = performDeleteRequest("/service/objects/id/" + (String) ret);
		prettyWrite(ret);
	}

	@Test
	public void editObject(){
		Object ret = performGetRequest("/service/objects/id/" + objectId);
		assertNotNull(ret);
		GeoObject object = stringToJson(jsonToString(ret), GeoObject.class);
		prettyWrite(object);
		String changedValue = "" + new Date().getTime();
		object.setDescription(changedValue);
		ret = performPutRequest("/service/objects/id/" + objectId, jsonToString(object));
		ret = performGetRequest("/service/objects/id/" + objectId);
		GeoObject newObject = stringToJson(jsonToString(ret), GeoObject.class);
		assertNotNull(newObject.getDescription());
		prettyWrite(newObject);
		assertEquals(changedValue, newObject.getDescription());
	}

	@Test
	public void getObjectDetails(){
		Object ret = performGetRequest("/service/objects/id/" + objectId);
		assertNotNull(ret);
		prettyWrite(ret);
	}

	@Test
	public void getObjectTypes(){
		Object ret = performGetRequest("/service/objects/types");
		assertNotNull(ret);
		List<?> types = stringToJson(jsonToString(ret), List.class);
		assertTrue(types.size() > 0);
		prettyWrite(types);
	}

	@Test
	public void listObjectsInRange(){
		Object ret = performGetRequest("/service/objects/nearby?lon=100.0&lat=100.0&dst=100.0");
		assertNotNull(ret);
		prettyWrite(ret);
	}
	
//	@SuppressWarnings({ "unchecked" })
	@Test
	public void addAndGetCommentsForGeoObject(){
		UserRef userRef = new UserRef();
		userRef.setId(userId);
		userRef.setUsername(username);
		Comment subComment = new Comment();
		subComment.setAuthor(userRef);
		subComment.setBody("test subcomment");
		Comment comment = new Comment();
		comment.setAuthor(userRef);
		comment.setBody("test comment");

		Object retr = performPostRequest("/service/objects/id/ChIJN5MjroBkjEcRMKa4TvKpEeU/comments", jsonToString(comment));
		commentId = (String)retr;
		Object ret2 = performPostRequest("/service/objects/id/" + objectId + "/comments?parentId=" + (String)retr, jsonToString(subComment));
		assertNotNull(ret2);
		prettyWrite(ret2);
//		Object ret = performPostRequest("/service/objects/" + objectId + "/comments", jsonToString(subComment));
//		assertNotNull(ret);
//		prettyWrite(ret);
//		entityManager.flush();
//		entityManager.clear();
//		Object ret3 = performGetRequest("/service/objects/" + objectId + "/comments");
//		assertNotNull(ret3);
//		prettyWrite(ret3);
//		List<Comment> cl = stringToJson(jsonToString(ret3), List.class);
//		assertTrue(cl.size() > 0);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void readComments(){
		entityManager.flush();
		entityManager.clear();
		Object ret3 = performGetRequest("/service/objects/id/550f05c4036419753dc9b3bf/comments");
		assertNotNull(ret3);
		prettyWrite(ret3);
		List<Comment> cl = stringToJson(jsonToString(ret3), List.class);
		assertTrue(cl.size() > 0);
	}
	
	@Test
	public void getObjectsForUserPositionLocalField(){
		String requestParamString = "";
		try {
			requestParamString = URLEncoder.encode("thoroughfare=Avenue Sainte-Cécile&subLocality=Meyrin"+
"&locality=Meyrin&postalCode=1217&countryCode=CH", "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		Position position = new Position();
//		position.setThoroughfare("Avenue Sainte-Cécile");
//		position.setSubLocality("Meyrin");
//		position.setLocality("Meyrin");
//		position.setPostalCode("1217");
//		position.setCountryCode("CH");
		Object ret = performGetRequest("/service/objects/area_objects?" + requestParamString);
		assertNotNull(ret);
		prettyWrite(ret, "objects in Meyrin");
	}


}